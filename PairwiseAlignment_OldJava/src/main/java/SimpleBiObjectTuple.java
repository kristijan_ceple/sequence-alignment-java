import java.util.ArrayList;
import java.util.List;

public class SimpleBiObjectTuple {

	private List<Direction> directionList = new ArrayList<>();
	private double price;
	
	public SimpleBiObjectTuple(List<Direction> directionList, double price) {
		super();
		this.directionList = directionList;
		this.price = price;
	}

	public List<Direction> getDirectionList() {
		return directionList;
	}

	public void setDirectionList(List<Direction> directionList) {
		this.directionList.addAll(directionList);
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((directionList == null) ? 0 : directionList.hashCode());
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleBiObjectTuple other = (SimpleBiObjectTuple) obj;
		if (directionList == null) {
			if (other.directionList != null)
				return false;
		} else if (!directionList.equals(other.directionList))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		return true;
	}
}
