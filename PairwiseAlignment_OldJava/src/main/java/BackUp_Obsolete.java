import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class BackUp_Obsolete {
	// Now find the max of them 3!
	// Need to update the currentState as well
	List<String> maxNumbers = findMaxAndUpdateCurrentState(fromLeft, fromTop, fromDiag);
	
	// Now let's update the current State
	Cell currentCell = NWTable[i][j];
	if(maxNumbers.size() == 1) {
		switch(maxNumbers.get(0)) {
			case "fromLeft" :
				nextState="Y"; 
				
				Direction firstLayerDirection = Direction.LEFT;
				// Now to calculate the secondLayerDirections
				List<Direction> secondLayerDirections = new ArrayList<Direction>();
				
				// Need the price so as to be able to test the new transition prices
				Double tmp = NWTable[i][j-1].getHighestPathScore();
				// Need the previous Directions and then get their states, so that we can test the new transition prices.
				Set<Direction> previousCellDirections = NWTable[i][j-1].getCameFrom().keySet();
				Set<String> previousCellStates = previousCellDirections.parallelStream()
						.map(direction -> switch(direction) {
							case LEFT -> "Y";
							case DIAGONAL_UP_LEFT -> upperSeq.charAt(currUpperIndex - 1) == lowerSeq.charAt(currLowerIndex - 1) ? "MTCH" : "MISS";
							case UP -> "X";
							default -> throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
						})
						.collect(Collectors.toSet());
				
				// Now we've got the states of the previous cell. Let's see which state/s yield/s the highest price
				// when transiting to the current cell
				tmp += 
				
				currentCell.putDirections(firstLayerDirection, secondLayerDirections);
				break;
			case "fromTop" : 
				nextState="X"; 
				
				firstLayerDirection = Direction.LEFT;
				
				currentCell.putDirections(firstLayerDirection, secondLayerDirections);
				
				break;
			case "fromDiag" :
				// nextState stays the same as in the above fromDiag case
				firstLayerDirection = Direction.LEFT;
				
				currentCell.putDirections(firstLayerDirection, secondLayerDirections);
				
				break; // ovo mi je cudno za next state
			default : throw new IllegalArgumentException();
		};
		
		// Now
	} else {
		// More paths - oh FUCK
		/*
		 * Need to note down each Path - why? Well when we are in cell [i, j], we will try to calculate the max value of the 3 directions from
		 * where we can arrive to this [i, j] cell and we have to note down each one of them. That is because the cells that follow after
		 * the cell [i, j] will depend on the STATE of the [i, j] cell. Why is that? Because we need to use the transmission matrix:
		 * State(previous cell) -> Current State. Current State of course depends on the movement - horizontal, vertical or diagonal.
		 * 
		 * The state of the previous cell depends also on the movement, but of the movement of the previous cell's northwestern neighbours.
		 * 
		 * So we can see that the current state differs if there are more possible paths. Therefore, even if all Paths have the same price,
		 * they may not yield the same price with the next state due to the differing currentState transmissionProbabilitiesMatrix values.
		 * 
		 * We therefore need to note down 2 previous comings - they way we came to this cell, and the way we came to one previous cell
		 * in orde to have the pre
		 */
		//
	}
	
}
}
}

// Create or update the entry nao!
//allPathsWithPrices.compute(Direction.DIAGONAL_UP_LEFT, (k, v) -> {
//	if(v == null) {
//		// Make a new list, and return it
//		List<Direction> directions = new ArrayList<>();
//		directions.add(prevCellCurrEntry.getKey());
//		
//		SimpleBiObjectTuple toReturn = new SimpleBiObjectTuple(directions, price); 
//		return toReturn;
//	} else {
//		// let's just get the list and append to it
//		List<Direction> directions = v.getDirectionList();
//		directions.add(prevCellCurrEntry.getKey());
//		return v;
//	}
//});

private static List<String> findMaxAndUpdateCurrentState(Double fromLeft, double fromTop, double fromDiag) {
	List<String> maxNumbers = new ArrayList<String>();
	
	if(fromLeft > fromDiag) {
		if(fromLeft > fromTop) {
			// fromLeft prevails!
			maxNumbers.add("fromLeft");
		} else if(fromLeft == fromTop) {
			maxNumbers.add("fromLeft");
			maxNumbers.add("fromTop");
		} else {
			maxNumbers.add("fromTop");
		}
	} else if(fromLeft <= fromDiag) {
		if(fromLeft > fromTop) {
			maxNumbers.add("fromLeft");
			maxNumbers.add("fromDiag");
		} else if(fromLeft == fromTop) {
			maxNumbers.add("fromLeft");
			maxNumbers.add("fromDiag");
			maxNumbers.add("fromTop");
		} else if(fromLeft < fromTop) {
			maxNumbers.add("fromTop");
		}
	}
	
	return maxNumbers;
}

/*
 * Since i only denotes the upperSeq WITHOUT gaps(-), we need something that shall help us include those gaps.
 * This is where the currUpperIndex comes into play
 */
int currUpperIndex = 0;

/*
 * The same as above except for the lower sequence
 */
int currLowerIndex = 0;

List<List<String>> stringPairs = new ArrayList<>();

// First let's get the last cell
Cell lastCell = NWTable[nrow-1][ncol-1];

// Let's follow now ehh
Map<Direction, List<Direction>> bestPaths = lastCell.getBestPathsWithoutPrices();
for(Entry<Direction, List<Direction>> entry : bestPaths.entrySet()) {
	// Each Direction in this map represents a viable Path
	// Hehe get it - VIABLE
	// That is because via, -ae, f. in latin means Path aajjaajaja
	// I should get a life
	// Let's make a pair of Strings
	List<String> pair = new ArrayList<>();
	
	String state = gapOrChars(entry.getKey());
	switch(state) {
		case "Y" -> { pair.add(String.valueOf(upperGaplessSeq.charAt(ncol-1))); pair.add("-"); }	// gap lower
			case "X" -> { pair.add("-"); pair.add(String.valueOf(lowerGaplessSeq.charAt(nrow-1))); }		// gap upper
			case "CHARS" -> { pair.add(String.valueOf(upperGaplessSeq.charAt(ncol-1))); pair.add(String.valueOf(lowerGaplessSeq.charAt(nrow-1))); }
			default -> throw new RuntimeException("gapOrChars weird return - should not ever happen");
	}
}

// We've started! This is done for every entry


����������������������������������			OLD ALGO		�������������������������������������
///*
//* If we moved from the left, that means there was a gap in the lower sequence
//* Transition from prevCellStates -> Y.
//*/
//// 1. fromLeft --> get the cell and its bestPathsWithouPrices map
//Map<Direction, List<Direction>> prevCellPaths = NWTable[i][j-1].getBestPathsWithoutPrices();
//Map<Direction, Map<Direction, Double>> allPathsWithPrices = NWTable[i][j].getAllPathsWithPrices();
//allPathsWithPrices.put(Direction.LEFT, new HashMap<>());		// Initialising the map
//
//// 2. Iterate over that map's entries
//for(Entry<Direction, List<Direction>> prevCellCurrEntry : prevCellPaths.entrySet()) {
//	// 2.a) Get the State from the Direction key
//	// Remember, GAP IN THE LOWER SEQUENCE!!!
////	String prevCellState = decodeState(prevCellCurrEntry.getKey(), upperGaplessSeq.charAt(j - 1), '-');
//	String prevCellState = reverseTransitionsMap.get(NWTable[i][j-1].getState());
//	String currCellState = prevCellState.equals("Y") || prevCellState.equals("L") ? "L" : "Y";
//	/*
//	 * Need to see whether the previous State is already Y or U. If yes, we need to go to U.
//	 * In all other cases, normal rules apply
//	 */
//	
//	// 2.b) Calculate the price
//	Double fromLeft = NWTable[i][j-1].getHighestPathScore();
//	double price = fromLeft
//			+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
//			+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))][GAP];
////	NWTable[i][j].setState(transitionsMap.get(currCellState));
//	statesMap.put(Direction.LEFT, transitionsMap.get(currCellState));
//	
////	// End state handling
////	if(i == nrow - 1 && j == ncol - 1) {
////		price += transitionProbabilitiesMatrix[transitionsMap.get(currCellState)][E];
////	}
//	
//	// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
//	allPathsWithPrices.get(Direction.LEFT).put(prevCellCurrEntry.getKey(), price);
//}

///*
//* If we moved from the top, that means there was a gap in the upper sequence
//* Transition from prevCellStates -> X.
//*/
//
//// 1. fromTop --> get the cell and its bestPathsWithouPrices map
////allPathsWithPrices = NWTable[i][j].getAllPathsWithPrices();
//prevCellPaths = NWTable[i-1][j].getBestPathsWithoutPrices();
//allPathsWithPrices.put(Direction.UP, new HashMap<>());		// Initialising the map
//
//// 2. Iterate over that map's entries
//for(Entry<Direction, List<Direction>> prevCellCurrEntry : prevCellPaths.entrySet()) {
//	// 2.a) Get the State from the Direction key
//	String prevCellState = reverseTransitionsMap.get(NWTable[i-1][j].getState());
//	String currCellState = prevCellState.equals("X") || prevCellState.equals("U") ? "U" : "X";
//	
//	// 2.b) Calculate the price
//	Double fromTop = NWTable[i-1][j].getHighestPathScore();
//	double price = fromTop
//			+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
//			+ emissionProbabilitiesMatrix[GAP][emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))];
////	NWTable[i][j].setState(transitionsMap.get(currCellState));
//	statesMap.put(Direction.UP, transitionsMap.get(currCellState));
//	
////	// End state handling
////	if(i == nrow - 1 && j == ncol - 1) {
////		price += transitionProbabilitiesMatrix[transitionsMap.get(currCellState)][E];
////	}
//	
//	// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
//	allPathsWithPrices.get(Direction.UP).put(prevCellCurrEntry.getKey(), price);

//// Check if the NWTable[1][1] - then we have to handle this a lil' bit differently!
//
////allPathsWithPrices = NWTable[i][j].getAllPathsWithPrices();
//prevCellPaths = NWTable[i-1][j-1].getBestPathsWithoutPrices();
//NWTable[i][j].getAllPathsWithPrices().put(Direction.DIAGONAL_UP_LEFT, new HashMap<>());		// Initialising the map
//
//// Need to see whether it's a match, or a mismatch! --> for the next State
//// 1. fromDiag --> get the cell and its bestPathsWithouPrices map
//if(i == 1 && j == 1) {
//	String prevCellState = "B";
//	boolean condition = upperGaplessSeq.charAt(j) == lowerGaplessSeq.charAt(i);
//	String currCellState = condition ? "MTCH" : "MISS";
//	 
//	// 2.b) Calculate the price
//	Double fromDiag = NWTable[i-1][j-1].getHighestPathScore();
//	double price = fromDiag
//			+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
//			+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))];
//	statesMap.put(Direction.DIAGONAL_UP_LEFT, transitionsMap.get(currCellState));
//	
//	// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
//	allPathsWithPrices.get(Direction.DIAGONAL_UP_LEFT).put(Direction.DIAGONAL_DOWN_RIGHT, price);		// DIAGONAL_DOWN_RIGHT is being used as to make this Begin Cell unique
//} else {
//	// 2. Iterate over that map's entries
//	for(Entry<Direction, List<Direction>> prevCellCurrEntry : prevCellPaths.entrySet()) {
//		// 2.a) Get the State from the Direction key
//		String prevCellState = reverseTransitionsMap.get(NWTable[i-1][j-1].getState());
//				
//		// Match or mismatch? 
//		boolean condition = upperGaplessSeq.charAt(j) == lowerGaplessSeq.charAt(i);
//		String currCellState = condition ? "MTCH" : "MISS";
//		 
//		// 2.b) Calculate the price
//		Double fromDiag = NWTable[i-1][j-1].getHighestPathScore();
//		double price = fromDiag
//				+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
//				+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))];
//		statesMap.put(Direction.DIAGONAL_UP_LEFT, transitionsMap.get(currCellState));
//		
////		// End state handling
////		if(i == nrow - 1 && j == ncol - 1) {
////			price += transitionProbabilitiesMatrix[transitionsMap.get(currCellState)][E];
////		}
//		
//		// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
//		allPathsWithPrices.get(Direction.DIAGONAL_UP_LEFT).put(prevCellCurrEntry.getKey(), price);
//	}
//}
//}

//// 3. Need to go through all the Paths now. Find the ones with the biggest price, and put them into the specialised
//// map which contains only the max-price Paths
//
//// First pass to discover the max Price
//Optional<Double> maxPriceOptional = allPathsWithPrices.values().parallelStream()
//		.map(Map::values)
//		.flatMap(Collection::parallelStream)
//		.max(Double::compare);
//
//if(maxPriceOptional.isEmpty()) {
//	// maxPrice not present -- what? This is obviously some weird edge case
//	// that should be further inspected
//	throw new RuntimeException("Empty/Not present maxPrice?");
//}
//double maxPrice = maxPriceOptional.get();
//
//// Second pass to add the best Paths
//Map<Direction, List<Direction>> bestPathsWithoutPrices = NWTable[i][j].getBestPathsWithoutPrices();
//// Let's now find the Paths that correspond to this newfound max price
//// Have to check each miniMap
//for(Entry<Direction, Map<Direction, Double>> entry : allPathsWithPrices.entrySet()) {
//	// Go through each minimap
//	for(Entry<Direction, Double> subEntry : entry.getValue().entrySet()) {
//		if(subEntry.getValue().equals(maxPrice)) {
//			// Add it to the bestPaths!
//			bestPathsWithoutPrices.compute(entry.getKey(), (k, v) -> {
//				Direction subDirection = subEntry.getKey();
//				
//				if(v == null) {
//					List<Direction> toPutMap = new ArrayList<>();
//					toPutMap.add(subDirection);
//					return toPutMap;
//				} else {
//					v.add(subDirection);
//					return v;
//				}
//			});
//			
//			NWTable[i][j].setState(statesMap.get(entry.getKey()));
//		}
//	}
//}
//
//NWTable[i][j].setHighestPathScore(maxPrice);
//}
//}

//sequencePairs,
//sequencePair);
//
//// Reversing the traceback
//for(List<String> pair : stringPairs) {
//// One possible alignment. Reverse the sequences!
//String upperSeqTmp = pair.get(PAIR_UPPER_SEQ_INDEX);
//String lowerSeqTmp = pair.get(PAIR_LOWER_SEQ_INDEX);
//
//String upperSeqTmpReversed = "";
//for(int i = upperSeqTmp.length() - 1; i >= 0; i--) {
//upperSeqTmpReversed = upperSeqTmpReversed.concat(String.valueOf(upperSeqTmp.charAt(i)));
//}
//
//String lowerSeqTmpReversed = "";
//for(int i = upperSeqTmp.length() - 1; i >= 0; i--) {
//lowerSeqTmpReversed = lowerSeqTmpReversed.concat(String.valueOf(lowerSeqTmp.charAt(i)));
//}
//
//// Now we got 'em reversed. Time to put them in their place. Literally lol
//pair.set(PAIR_UPPER_SEQ_INDEX, upperSeqTmpReversed);
//pair.set(PAIR_LOWER_SEQ_INDEX, lowerSeqTmpReversed);
//}
//
//// Just print them out now
//System.out.println("Possible paths:");
//for(List<String> pair : stringPairs) {
//System.out.println(pair.get(PAIR_UPPER_SEQ_INDEX));
//System.out.println(pair.get(PAIR_LOWER_SEQ_INDEX));
//
//// Let's print the delimiter
//System.out.println("\n");
//}
//

/**
* Goes back. Generates a list of String pairs. In SequencePair, on index 0 lies the upper sequence,
* and on index 1 lies the lower sequence.
* @param nWTable
*/
@SuppressWarnings("preview")
private static List<List<String>> traceback(Cell[][] NWTable, int i, int j, String upperGaplessSeq, String lowerGaplessSeq, List<List<String>> sequencePairs, List<String> sequencePair) {
	// The breaking statement - check if we've arrived to the beginning
	if(i == 0 && j == 0) {
		// Now add the String pair to the list of string pairs, and then return it
		sequencePairs.add(sequencePair);
		return sequencePairs;
	}
	
	Cell currentCell = NWTable[i][j];
	Map<Direction, List<Direction>> bestPaths = currentCell.getBestPathsWithoutPrices();
	// Let's follow now ehh
	
	// First check whether there's some branching happening
	boolean branch = false;
	if(bestPaths.size() > 1) {
		// Oh go there's a branching happening
		/*
		 * What's the point of branching you might ask?
		 * That much is clear --> in order to form different Paths. The better question is: how to achieve that? What must be done? Well we have to initialise
		 * two new Strings, and copy into them the content of the String that the traceback has received. This is because 1 Path is actually branching into 2
		 * new Paths, and therefore we need to preserve the pre-branch part, while still allowing the Paths to be 2/3 new Strings to which it is possible to
		 * concatenate onto. At the end of the traceback the recursion shall add all these paths to the List.
		 */
		branch = true;
	}
	
	for(Entry<Direction, List<Direction>> entry : bestPaths.entrySet()) {
		String state = reverseTransitionsMap.get(currentCell.getState());
		
		// Will have to handle branching here. If there's branching happening we will need 2/3 new Pairs.
		List<String> pairSeqRef = sequencePair;
		
		if(branch) {
			// Let's make a new Pair for each of the entries
			List<String> newPair = new ArrayList<>();
			String newUpper = "", newLower = "";
			
			newUpper = newUpper.concat(sequencePair.get(PAIR_UPPER_SEQ_INDEX));
			newLower = newLower.concat(sequencePair.get(PAIR_LOWER_SEQ_INDEX));
			
			newPair.add(newUpper);
			newPair.add(newLower);
			
			// Okay, got em now. Update the reference to the new Pair
			pairSeqRef = newPair;
		}
		
		String tmpUpperSeq = null;
		String tmpLowerSeq = null;
		switch(state) {
			case "Y":
			case "L":
				tmpUpperSeq = pairSeqRef.get(PAIR_UPPER_SEQ_INDEX).concat(String.valueOf(upperGaplessSeq.charAt(j)));
				tmpLowerSeq = pairSeqRef.get(PAIR_LOWER_SEQ_INDEX).concat("-");	// gap lower
				
				break;

			case "X": 
			case "U": 
				tmpUpperSeq = pairSeqRef.get(PAIR_UPPER_SEQ_INDEX).concat("-");
				tmpLowerSeq = pairSeqRef.get(PAIR_LOWER_SEQ_INDEX).concat(String.valueOf(lowerGaplessSeq.charAt(i)));
				
				break;	// gap upper
			case "MTCH": 
			case "MISS":
				tmpUpperSeq = pairSeqRef.get(PAIR_UPPER_SEQ_INDEX).concat(String.valueOf(upperGaplessSeq.charAt(j)));
				tmpLowerSeq = pairSeqRef.get(PAIR_LOWER_SEQ_INDEX).concat(String.valueOf(lowerGaplessSeq.charAt(i)));
				
				break;
			default:
				throw new RuntimeException("gapOrChars weird return - should not ever happen");
		}
		
		pairSeqRef.set(PAIR_UPPER_SEQ_INDEX, tmpUpperSeq);
		pairSeqRef.set(PAIR_LOWER_SEQ_INDEX, tmpLowerSeq);
	
		// We've concatenated the Paths right now. Continue the recursion now
		// Depending on the Direction, need to calculate the new i and j values
		Direction toCalculate = entry.getKey();
		int newi = i, newj = j;
		switch(toCalculate) {
			case LEFT:
				newj = j - 1;
				break;
			case DIAGONAL_UP_LEFT:
				newi = i -1; newj = j - 1;
				break;
			case UP:
				newi = i - 1;
				break;
			default:
				throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
		}
		
		return traceback(NWTable, newi, newj, upperGaplessSeq, lowerGaplessSeq, sequencePairs, pairSeqRef);
	}
	
	return null;
}