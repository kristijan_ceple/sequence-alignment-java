import java.util.ArrayList;
import java.util.List;

/**
 * Used for the NW table. Stores data in a Map.
 * @author kikyy99
 *
 */
public class Cell {
	
	private double highestPathScore;
	private List<State> highestScoreStates = new ArrayList<State>(1);
	
	/**
	 * Empty constructor
	 */
	public Cell() {
		super();
	}

	/**
	 * Constructor with a single value that will be bound to this cell(reference saving)
	 * 
	 * @param val The value whose reference will be saved
	 */
	public Cell(double highestPathScore) {
		this.highestPathScore = highestPathScore;
	}

	public double getHighestPathScore() {
		return highestPathScore;
	}


	public void setHighestPathScore(double highestPathScore) {
		this.highestPathScore = highestPathScore;
	}

	public List<State> getHighestScoreStates() {
		return highestScoreStates;
	}

	public void setHighestScoreStates(List<State> highestScoreStates) {
		this.highestScoreStates = highestScoreStates;
	}
	
	public void addState(State toAddState) {
		this.highestScoreStates.add(toAddState);
	}

	@Override
	public String toString() {
		return "Cell [highestPathScore=" + highestPathScore + ", highestScoreStates=" + highestScoreStates + "]";
	}
	
}
