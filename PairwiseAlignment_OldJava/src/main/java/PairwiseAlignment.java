import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.OptionalDouble;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cern.colt.list.DoubleArrayList;

public class PairwiseAlignment {

	public static int EMISSIONS_MATRIX_CONST = 5;		// A, T, G, C, and -
	public static int TRANSITIONS_MATRIX_CONST = 8; // B, E, MATCH. MISS, X, Y,
                                                // U (repeated upper gap) and L (repeated lower gap)

	public static int progressCounter = 0;
	public static int progressTimer = 0;
	
	public static String PATH_CONST = "HIV1_REF_2010_genome_DNA.fasta";
//	public static String PATH_CONST = "C:/Users/Bruno/Desktop/hiv.txt";
	public static int GAP_POINTS = -1;
	public static int MATCH_POINTS = -2;
	
	private static int emissionsMatrix[][] = new int[EMISSIONS_MATRIX_CONST][EMISSIONS_MATRIX_CONST];
	private static int transitionMatrix[][] = new int[TRANSITIONS_MATRIX_CONST][TRANSITIONS_MATRIX_CONST];
	private static double emissionProbabilitiesMatrix[][] = new double[EMISSIONS_MATRIX_CONST][EMISSIONS_MATRIX_CONST];
	private static double transitionProbabilitiesMatrix[][] = new double[TRANSITIONS_MATRIX_CONST][TRANSITIONS_MATRIX_CONST];
	
	private static Map<String, Integer> emissionsMap = new HashMap<>(5);
	private static Map<Integer, String> reverseEmissionsMap = new HashMap<>(5);
	
	private static final int A = 0;
	private static final int C = 1;
	private static final int G = 2;
	private static final int T = 3;
	private static final int GAP = 4;
	
	private static final int PAIR_UPPER_SEQ_INDEX = 0;
	private static final int PAIR_LOWER_SEQ_INDEX = 1;
	
	/**
	 * The comparison constant for doubles
	 */
	private static final double EPSILON = 1E-6;
	/**
	 * The progress percentage multiples
	 */
	private static final int PART = 5;
	
	public static void main(String[] args) {
		
		// First fill the emissionsMap with values
		emissionsMap.put("A", 0);
		emissionsMap.put("C", 1);
		emissionsMap.put("G", 2);
		emissionsMap.put("T", 3);
		emissionsMap.put("-", 4);
		
		// Fill the reverse map
		reverseEmissionsMap.put(0, "A");
		reverseEmissionsMap.put(1, "C");
		reverseEmissionsMap.put(2, "G");
		reverseEmissionsMap.put(3, "T");
		reverseEmissionsMap.put(4, "-");
		
		// Let's parse the arguments first
		if(args.length == 0) {
			// Run the default course
			defaultCourse();
		} else  if(args.length == 1){
			argumentCourse(args);
		} else {
			if(args.length != 3) {
				throw new IllegalArgumentException("Invalid number of cmd arguments!");
			} else {
				argumentsCourse(args);
			}
		}
	}
	
	private static void argumentCourse(String[] args) {
		String learnPath = args[0];
		if(learnPath.length() == 0) {
			throw new IllegalArgumentException("One or more empty arguments.");
		}
		
		String file=learnPath;
		Path path = Paths.get(file);
		
		List<String> lines = fileRazvrstajUPoljeStringova(path);

//		System.out.println(lines.get(38));
//		System.out.println("Charlie");
		
		napraviMatricuEmisija(lines);
		napraviMatricuTranzicija(lines);
		
		System.out.println("Matrica emisija");
		ispisiMatricu(emissionsMatrix);
		
		System.out.println("Matrica tranzicija");
		ispisiMatricu(transitionMatrix);
		
		izracunajVjerojatnostiEmisija();
		System.out.println("Matrica vjerojatnosti emisija - logaritam");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		
		izracunajVjerojatnostiTranzicija();
		System.out.println("Matrica vjerojatnosti tranzicija - logaritam");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		// Need to make log odds to the tables
		logOddsMatrix(emissionProbabilitiesMatrix);
		logOddsMatrix(transitionProbabilitiesMatrix);
		System.out.println("Matrica vjerojatnosti emisija - logaritam");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		System.out.println("Matrica vjerojatnosti tranzicija - logaritam");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		NeedlemanWunschPair("AMTCY","ARUC");
	}

	public static void argumentsCourse(String[] args) {
		String firstArg = args[0];
		String secondArg = args[1];
		String thirdArg = args[2];
		
		if(firstArg.length() == 0 || secondArg.length() == 0) {
			throw new IllegalArgumentException("One or more empty arguments.");
		}
		
		switch(thirdArg) {
		case "SEQS":
			sequences(firstArg, secondArg);
			break;
		case "FILES":
			files(firstArg, secondArg);
			break;
		default:
			// Let's check if the string matches the regex
			Pattern pattern = Pattern.compile("^FILES_(\\d+)$");
			Matcher matcher = pattern.matcher(thirdArg);
			if(matcher.matches()) {
				int alignmentsNum = Integer.parseInt(matcher.group(1));
				files(firstArg, secondArg, alignmentsNum);
			} else {
				throw new IllegalArgumentException("Neither SEQS nor FILES arg passed!");
			}
			
		}
	}
	
	private static void sequences(String firstSeq, String secondSeq) {
		String file = PATH_CONST;
		Path path = Paths.get(file);
		
		List<String> lines = fileRazvrstajUPoljeStringova(path);

//		System.out.println(lines.get(38));
//		System.out.println("Charlie");
		
		napraviMatricuEmisija(lines);
		napraviMatricuTranzicija(lines);
		
		System.out.println("Matrica emisija");
		ispisiMatricu(emissionsMatrix);
		
		System.out.println("Matrica tranzicija");
		ispisiMatricu(transitionMatrix);
		
		izracunajVjerojatnostiEmisija();
		System.out.println("Matrica vjerojatnosti emisija");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		
		izracunajVjerojatnostiTranzicija();
		System.out.println("Matrica vjerojatnosti tranzicija");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		// Need to make log odds to the tables
		logOddsMatrix(emissionProbabilitiesMatrix);
		logOddsMatrix(transitionProbabilitiesMatrix);
		System.out.println("Matrica vjerojatnosti emisija - logaritam");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		System.out.println("Matrica vjerojatnosti tranzicija - logaritam");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		NeedlemanWunschPair(firstSeq, secondSeq);
	}

	private static void files(String learnPath, String alignPath) {
		String file=learnPath;
		Path path = Paths.get(file);
		
		List<String> lines = fileRazvrstajUPoljeStringova(path);

//		System.out.println(lines.get(38));
//		System.out.println("Charlie");
		
		napraviMatricuEmisija(lines);
		napraviMatricuTranzicija(lines);
		
		System.out.println("Matrica emisija");
		ispisiMatricu(emissionsMatrix);
		
		System.out.println("Matrica tranzicija");
		ispisiMatricu(transitionMatrix);
		
		izracunajVjerojatnostiEmisija();
		System.out.println("Matrica vjerojatnosti emisija");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		
		izracunajVjerojatnostiTranzicija();
		System.out.println("Matrica vjerojatnosti tranzicija");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		// Need to make log odds to the tables
		logOddsMatrix(emissionProbabilitiesMatrix);
		logOddsMatrix(transitionProbabilitiesMatrix);
		System.out.println("Matrica vjerojatnosti emisija - logaritam");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		System.out.println("Matrica vjerojatnosti tranzicija - logaritam");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		// Here need to read the second file into a second lines
		file = alignPath;
		path = Paths.get(file);
		List<String> toAlignSeqs = fileRazvrstajUPoljeStringova(path);
		
		double time1 = System.currentTimeMillis();
		int n = toAlignSeqs.size();
		for(int i = 0; i < n - 1; i++) {
			for(int j = i + 1; j < n; j++) {
				String firstSeq = toAlignSeqs.get(i);
				String secondSeq = toAlignSeqs.get(j);
				NeedlemanWunschPair(firstSeq, secondSeq);
			}
		}
		double time2 = System.currentTimeMillis();
		double totalTime = time2 - time1;
		System.out.println("Total runtime: " + totalTime + " milliseconds.");
		System.out.println("               " + totalTime/1000 + " seconds.");
		System.out.println("               " + totalTime/(1000 * 60) + " minutes.");
	}
	
	private static void files(String learnPath, String alignPath, int alignmentsNum) {
		String file=learnPath;
		Path path = Paths.get(file);
		
		List<String> lines = fileRazvrstajUPoljeStringova(path);

//		System.out.println(lines.get(38));
//		System.out.println("Charlie");
		
		napraviMatricuEmisija(lines);
		napraviMatricuTranzicija(lines);
		
		System.out.println("Matrica emisija");
		ispisiMatricu(emissionsMatrix);
		
		System.out.println("Matrica tranzicija");
		ispisiMatricu(transitionMatrix);
		
		izracunajVjerojatnostiEmisija();
		System.out.println("Matrica vjerojatnosti emisija");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		
		izracunajVjerojatnostiTranzicija();
		System.out.println("Matrica vjerojatnosti tranzicija");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		// Need to make log odds to the tables
		logOddsMatrix(emissionProbabilitiesMatrix);
		logOddsMatrix(transitionProbabilitiesMatrix);
		System.out.println("Matrica vjerojatnosti emisija - logaritam");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		System.out.println("Matrica vjerojatnosti tranzicija - logaritam");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		// Here need to read the second file into a second lines
		file=alignPath;
		path = Paths.get(file);
		List<String> toAlignSeqs = fileRazvrstajUPoljeStringova(path);

		// Check that alignmentsNum isn't too alrge
		if(alignmentsNum > toAlignSeqs.size()) {
			throw new IllegalArgumentException("Parameter alignmentsNum is too large - not enough sequences to be aligned!");
		}
		
		double time1 = System.currentTimeMillis();
		for(int i = 0; i < alignmentsNum - 1; i++) {
			for(int j = i + 1; j < alignmentsNum; j++) {
				String firstSeq = toAlignSeqs.get(i);
				String secondSeq = toAlignSeqs.get(j);
				NeedlemanWunschPair(firstSeq, secondSeq);
			}
		}
		double time2 = System.currentTimeMillis();
		double totalTime = time2 - time1;
		System.out.println("Total runtime: " + totalTime + " milliseconds.");
		System.out.println("               " + totalTime/1000 + " seconds.");
		System.out.println("               " + totalTime/(1000 * 60) + " minutes.");
	}

	public static void defaultCourse() {
		String file=PATH_CONST;
		Path path = Paths.get(file);
		
		List<String> lines = fileRazvrstajUPoljeStringova(path);

//		System.out.println(lines.get(38));
//		System.out.println("Charlie");
		
		napraviMatricuEmisija(lines);
		napraviMatricuTranzicija(lines);
		
		System.out.println("Matrica emisija");
		ispisiMatricu(emissionsMatrix);
		
		System.out.println("Matrica tranzicija");
		ispisiMatricu(transitionMatrix);
		
		izracunajVjerojatnostiEmisija();
		System.out.println("Matrica vjerojatnosti emisija");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		
		izracunajVjerojatnostiTranzicija();
		System.out.println("Matrica vjerojatnosti tranzicija");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		// Need to make log odds to the tables
		logOddsMatrix(emissionProbabilitiesMatrix);
		logOddsMatrix(transitionProbabilitiesMatrix);
		System.out.println("Matrica vjerojatnosti emisija - logaritam");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");
		System.out.println("Matrica vjerojatnosti tranzicija - logaritam");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");
		
		NeedlemanWunschPair("AMTCY","ARUC");
	}
	
	private static String gapOrChars(Direction toDecode) {
		switch(toDecode) {
			case LEFT:
				return "Y";
			case DIAGONAL_UP_LEFT:
				return "CHARS";
			case UP:
				return "X";
			default:
				throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
		}
	}

	private static String decodeState(Direction toDecode, char upperChar, char lowerChar) {
		switch(toDecode) {
			case LEFT:
				return "Y";
			case DIAGONAL_UP_LEFT:
				return upperChar == lowerChar ? "MTCH" : "MISS";
			case UP:
				return "X";
			case DIAGONAL_DOWN_RIGHT:
				return "B";
			default:
				throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
		}
	}

	private static String decodeState(Direction toDecode) {
		switch(toDecode) {
			case LEFT:
				return "Y";
			case DIAGONAL_UP_LEFT:
				return "MTCH/MISS";
			case UP:
				return "X";
			case DIAGONAL_DOWN_RIGHT:
				return "B";
			default:
				throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
		}
	}
	
	private static void NeedlemanWunschPair(String upperGaplessSeq, String lowerGaplessSeq) {
		// Beginning check
		Objects.requireNonNull(upperGaplessSeq);
		Objects.requireNonNull(lowerGaplessSeq);
		
		if(upperGaplessSeq.length() == 0 || lowerGaplessSeq.length() == 0) {
			throw new IllegalArgumentException("Empty string passed!");
		}
		
		double totalTime = 0;
		double timeTmp = 0;
		
		System.out.println("Comparing:");
		System.out.println(upperGaplessSeq);
		System.out.println(lowerGaplessSeq + "\n");
		
		// Need to form a table first - find out its dimensions
		upperGaplessSeq = "-" + upperGaplessSeq.replaceAll("[^ACGT]", "");
		lowerGaplessSeq = "-" + lowerGaplessSeq.replaceAll("[^ACGT]", "");
		
		int ncol = upperGaplessSeq.length(), nrow = lowerGaplessSeq.length();
		Cell[][] NWTable = new Cell[nrow][ncol];		// This shall be used as the table
		
		System.out.println("Initialising the table Cell objects...");
		int progPart = nrow * ncol * PART / 100 ;

		long time1 = System.currentTimeMillis();
		// Let's first initialise the Cells
		for(int i = 0; i < nrow; i++) {
			for(int j = 0; j < ncol; j++) {
				countInit(progPart);
				NWTable[i][j] = new Cell();
			}
		}
		long time2 = System.currentTimeMillis();
		timeTmp = (time2 - time1);
		totalTime += timeTmp;			// Add cell initialisation time to total time
		System.out.println("Cell initialisation execution time: " + timeTmp + " milliseconds.\n");
		
		// Reset the counter and the time
		progressCounter = 0;
		progressTimer = 0;
		
		System.out.println("Calculating table values...");
		time1 = System.currentTimeMillis();
		
		// The algorithm calculation part
		/*
		 * We start in the Begin state, and are located in the upper leftmost cell. From there, we calculate
		 * first the 0th row and the 0th column, and then move on to the others
		 */
		State currentState = State.B;
		State nextState;
		Cell currCell = NWTable[0][0];
		
		currCell.setHighestPathScore(0);
		currCell.addState(currentState);
		
		// ��������������������� Let's fill the first row	����������������������//
		currCell = NWTable[0][1];
		nextState = State.Y;
		double priceTmp = transitionProbabilitiesMatrix[currentState.getStateNum()][nextState.getStateNum()]
				+ emissionProbabilitiesMatrix[emissionsMap.get("-")][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(1)))];
		currCell.setHighestPathScore( 				// B -> Y
							priceTmp
						);
		currCell.addState(nextState);
		count(progPart);
		
		if(upperGaplessSeq.length() >= 3) {
			currentState = State.Y;		// Consecutive gaps!
			nextState = State.L;
			
			currCell = NWTable[0][2];
			priceTmp = transitionProbabilitiesMatrix[currentState.getStateNum()][nextState.getStateNum()]
					+ emissionProbabilitiesMatrix[emissionsMap.get("-")][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(2)))]
					+ NWTable[0][1].getHighestPathScore();
			currCell.setHighestPathScore(
								priceTmp
							);
			currCell.addState(nextState);
			count(progPart);
			
			if(upperGaplessSeq.length() >= 4) {
				// Now we're in state L, and the next state is L as well --> CONSECUTIVE GAPS
				currentState = nextState;
				for( int j = 3; j < ncol; j++) {
					// The gaps in the lower sequence(transitions L -> L)
					currCell = NWTable[0][j];
					priceTmp = transitionProbabilitiesMatrix[currentState.getStateNum()][nextState.getStateNum()]
							+ emissionProbabilitiesMatrix[emissionsMap.get("-")][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))]
							+ NWTable[0][j-1].getHighestPathScore();
					currCell.setHighestPathScore(
										priceTmp
									);
					currCell.addState(nextState);
					count(progPart);
				}
			}
		}
		
		// ��������������������� Let's fill the first column	����������������������//
		currentState = State.B;
		nextState = State.X;
		currCell = NWTable[1][0];
		priceTmp = transitionProbabilitiesMatrix[currentState.getStateNum()][nextState.getStateNum()]
				+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(1)))][emissionsMap.get("-")];
		currCell.setHighestPathScore( 				// B -> X
							priceTmp
						);
		currCell.addState(nextState);
		count(progPart);
		
		if(lowerGaplessSeq.length() >= 3) {
			currCell = NWTable[2][0];
			currentState = State.X;
			nextState = State.U;	// Next state shall be consecutive gaps
			priceTmp = transitionProbabilitiesMatrix[currentState.getStateNum()][nextState.getStateNum()]
					+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(2)))][emissionsMap.get("-")]
					+ NWTable[1][0].getHighestPathScore();
			currCell.setHighestPathScore(
						priceTmp
					);
			currCell.addState(nextState);
			count(progPart);
			
			if(lowerGaplessSeq.length() >= 4) {
				// Now we're in state U, and the next state is U as well --> CONSECUTIVE GAPS
				currentState = nextState;
				for( int i = 3; i < nrow; i++ ) {
					currCell = NWTable[i][0];
					priceTmp = transitionProbabilitiesMatrix[currentState.getStateNum()][nextState.getStateNum()]
							+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][emissionsMap.get("-")]
							+ NWTable[i-1][0].getHighestPathScore(); 
					currCell.setHighestPathScore(
								priceTmp
							);
					currCell.addState(nextState);
					count(progPart);
				}
			}
		}
		
		// �������������������������������������		Values cache		������������������������������������������
		Cell prevCell;
		State currCellState;
		List<State> bestPrevCellStates;
		double price;
		double dirPriceTmp;
		DoubleArrayList coltPriceList;
		DoubleArrayList coltTmpPriceList;
		OptionalDouble maxPriceOptional;
		double maxPrice;
		Map<State, DoubleArrayList> statesMap = new HashMap<>(3);
		// �������������������������������������		Values cache		������������������������������������������
		
		// Time to go row by row and parse each cell - gather 3 values, and select the maximum of them
		for(int i = 1; i < nrow; i++) {
			for(int j = 1; j < ncol; j++) {
				// Do the algo here
				/*
				 * This is the algorithm:
				 * 1. Depending whether it's about fromLeft, fromTop or fromDiag - get that cell's cameFrom map.
				 * Do this for all the 3 neigbouring Cells.
				 * 2. Need to iterate over this map:
				 * 	a) Discover that Cell's State using the Direction key of the map
				 * 	b) Calculate the total corresponding price for the current Cell
				 * 	c) Note the price and the path down into current Cell's cameFromWithPrices map
				 * 
				 * 3. After this has been done for all 3 neighbouring Cells, it is time to find the maximum price.
				 * That price needs to be noted down in this cell's cameFrom map.
				 * 
				 * Note the difference between the cameFrom and cameFromWithPrices maps. The cameFrom map features only the paths
				 * with the highest(and in this case, equal) price, while the cameFromWithPrices map features all the paths ever tested and calculated.
				 */
				
				statesMap.clear();
				currCell = NWTable[i][j];
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Let's do the LEFT case		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

				// 1. Iterate over the prevCell states and calculate the price
				prevCell = NWTable[i][j - 1];
				bestPrevCellStates = prevCell.getHighestScoreStates();
				for(State prevCellState : bestPrevCellStates) {
					// Need to calculate the price first
					// 2.a The current cell state will depend on the previous cell State!
					currCellState = prevCellState.equals(State.Y) ? State.L : State.Y; 
					
					// 2.b Price calculation
					dirPriceTmp = prevCell.getHighestPathScore();
					price = dirPriceTmp
							+ transitionProbabilitiesMatrix[prevCellState.getStateNum()][currCellState.getStateNum()]
							+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))][GAP];
					
					// 2.c Add this price to the map of all directions and prices
					coltPriceList = statesMap.get(currCellState);
					if(coltPriceList == null) {
						// Create a new list and add an element to it
						coltTmpPriceList = new DoubleArrayList(1);
						coltTmpPriceList.add(price);
						statesMap.put(currCellState, coltTmpPriceList);
					} else {
						// Update the existing list
						coltPriceList.add(price);
					}
				}
				
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Let's do the TOP case		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

				// 1. Iterate over the prevCell states and calculate the price
				prevCell = NWTable[i - 1][j];
				bestPrevCellStates = prevCell.getHighestScoreStates();
				for(State prevCellState : bestPrevCellStates) {
					// Need to calculate the price first
					// 2.a The current cell state will depend on the previous cell State!
					currCellState = prevCellState.equals(State.X) ? State.U : State.X; 
					
					// 2.b Price calculation
					dirPriceTmp = prevCell.getHighestPathScore();
					price = dirPriceTmp
							+ transitionProbabilitiesMatrix[prevCellState.getStateNum()][currCellState.getStateNum()]
							+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][GAP];
					
					// 2.c Add this price to the map of all directions and prices
					coltPriceList = statesMap.get(currCellState);
					if(coltPriceList == null) {
						// Create a new list and add an element to it
						coltTmpPriceList = new DoubleArrayList(1);
						coltTmpPriceList.add(price);
						statesMap.put(currCellState, coltTmpPriceList);
					} else {
						// Update the existing list
						coltPriceList.add(price);
					}
				}
				
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Let's do the DIAGONAL case		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

				// 1. Iterate over the prevCell states and calculate the price
				prevCell = NWTable[i - 1][j - 1];
				bestPrevCellStates = prevCell.getHighestScoreStates();
				for(State prevCellState : bestPrevCellStates) {
					// Need to calculate the price first
					/*
					 * 2.a The current cell state will depend not on the previous cell State,
					 * but rather whether the sequence pair is a match or mismatch!
					 */
					currCellState = upperGaplessSeq.charAt(j) == lowerGaplessSeq.charAt(i)
							? State.MTCH : State.MISS;
					
					// 2.b Price calculation
					dirPriceTmp = prevCell.getHighestPathScore();
					price = dirPriceTmp
							+ transitionProbabilitiesMatrix[prevCellState.getStateNum()][currCellState.getStateNum()]
							+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))][emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))];
					
					// 2.c Add this price to the map of all directions and prices
					coltPriceList = statesMap.get(currCellState);
					if(coltPriceList == null) {
						// Create a new list and add an element to it
						coltTmpPriceList = new DoubleArrayList(1);
						coltTmpPriceList.add(price);
						statesMap.put(currCellState, coltTmpPriceList);
					} else {
						// Update the existing list
						coltPriceList.add(price);
					}
				}
				
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		ON TO STEP 3		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

				// Now go through all the entries in the prices map, and mark down the most likely States
				// First need to discover the max price

				maxPriceOptional = statesMap.entrySet().parallelStream()
						.flatMapToDouble(entry -> {
							DoubleArrayList values = entry.getValue();
							return Arrays.stream(values.elements());
						}).max();
				
				if(maxPriceOptional.isPresent()) {
					maxPrice = maxPriceOptional.getAsDouble();
				} else {
					throw new RuntimeException("Max price couldn't be calculated!");
				}
				
//				coltPriceList.sort();
//				double maxPrice = coltPriceList.get(coltPriceList.size() - 1);
				
				// Excellent, now we have the max price. Set it into this Cell as the highest price
				currCell.setHighestPathScore(maxPrice);

				// Now discover the States that have got this max Price, and add them to this Cell's list of States
				for(Entry<State, DoubleArrayList> currEntry : statesMap.entrySet()) {
					for(double currValue : currEntry.getValue().elements()) {
						if( Math.abs(currValue - maxPrice) <= EPSILON) {
							// Found the State
							currCell.addState(currEntry.getKey());
						}
					}
				}
				
				// Now that this Cell's been done - time to count!
				count(progPart);
			}
		}
		
		// All done. The trace back shall discover the rest!
		// First reset the counter
		progressCounter = 0;
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Traceback		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		time2 = System.currentTimeMillis();
		timeTmp = time2 - time1;
		totalTime += timeTmp;			// Add algorithm time to total time!
		double secondsExec = timeTmp/1000;
		System.out.println("Algorithm execution time: " + secondsExec + " seconds.");
		System.out.println("Algorithm execution time: " + secondsExec/60 + " minutes.");
		System.out.println("Table calculation complete. Performing traceback...\n");
		
		// Once that is done we will have the table filled. Now need to follow those directions back
		List<StringBuilder> sequencePair = new ArrayList<>(2);
		
		// Prepare the first two Strings
		sequencePair.add(new StringBuilder());
		sequencePair.add(new StringBuilder());
		
		time1 = System.currentTimeMillis();
		sequencePair = traceback(NWTable, nrow - 1, ncol - 1, upperGaplessSeq, lowerGaplessSeq, sequencePair, State.E);
		time2 = System.currentTimeMillis();
		timeTmp = time2 - time1;
		totalTime += timeTmp;			// Add traceback time to total time!
		System.out.println("Traceback execution time: " + timeTmp + " milliseconds.\n");
		System.out.println("Two sequence alignment results:");
		
		System.out.println(sequencePair.get(PAIR_UPPER_SEQ_INDEX).reverse());
		System.out.println(sequencePair.get(PAIR_LOWER_SEQ_INDEX).reverse());
		System.out.println("Total pair runtime: " + totalTime + " milliseconds.");
		System.out.println("                    " + totalTime/1000 + " seconds.");
		System.out.println("                    " + totalTime/(1000 * 60) + " minutes.");
		System.out.println("\n\n�����������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������\n\n");
	}
    
	public static void countInit(int progPart) {
		// The counter part
		progressTimer++;
		if(progressTimer == progPart) {
			percentageReportiInit();
			progressTimer = 0;
		}
	}
	
	public static void count(int progPart) {
		// The counter part
		progressTimer++;
		if(progressTimer == progPart) {
			percentageReport();
			progressTimer = 0;
		}
	}
	
	private static void percentageReportiInit() {
		if(progressCounter * PART >= 100) {
			return;
		}
		
		System.out.println(++progressCounter * PART + "% table initialised.");
	}

	private static void percentageReport() {
		if(progressCounter * PART >= 100) {
			return;
		}
		
		System.out.println(++progressCounter * PART + "% table calculated.");
	}
	
	/**
	 * @param NWTable self-explanatory
	 * @param i row index
	 * @param j column index
	 * @param upperGaplessSeq self-explanatory
	 * @param lowerGaplessSeq self-explanatory
	 * @param sequencePair the most probable Path that is being formed
	 * @return One of the most probable Paths
	 */
	private static List<StringBuilder> traceback(Cell[][] NWTable, int i, int j, 
			String upperGaplessSeq, String lowerGaplessSeq, 
			List<StringBuilder> sequencePair, State backwardsState) {
		
		if(i == 0 && j == 0) {
			return sequencePair;	// Empty state, no need to add anything, just return the value
		}
		
		Cell currCell = NWTable[i][j];
		List<State> currCellStates = currCell.getHighestScoreStates();
		double maxPrice = currCell.getHighestPathScore();
		for(State currState : currCellStates) {
			// Chaining the states
			if(backwardsState != State.E && currState != backwardsState) {
				continue;
			}
			
			Direction whereTo = currState.getWhereFrom();
			State prevMatchingState = null;			
			
			// Need to take a look at the nextState var here
			switch(whereTo) {
			case DIAGONAL_UP_LEFT:
				/*
				 * So, we came from the left. And in the left cell we came from the nextState var. Maybe?
				 * Let's take the max cell price, and check where did we come from into that previous cell ehh?
				 */
				Cell prevCell = NWTable[i-1][j-1];
				List<State> prevCellStates = prevCell.getHighestScoreStates();
				double prevMaxPrice = prevCell.getHighestPathScore();
				
				/*
				 * This is the map where we shall put the values into. What values you ask?
				 * Well we have to calculate the prices for 
				 */
				
				// Now we need to calculate which states would match this max price???
				for(State prevState : prevCellStates) {
					// do the calc here
					double priceTmp = prevMaxPrice + emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))]
							+ transitionProbabilitiesMatrix[prevState.getStateNum()][currState.getStateNum()];
					if( Math.abs(priceTmp - maxPrice) <= EPSILON ) {
						// One of the fitting states --> Pick it!
						prevMatchingState = prevState;
						break;
					}
				}
				break;
			case LEFT:
				prevCell = NWTable[i][j-1];
				prevCellStates = prevCell.getHighestScoreStates();
				prevMaxPrice = prevCell.getHighestPathScore();
				
				/*
				 * This is the map where we shall put the values into. What values you ask?
				 * Well we have to calculate the prices for 
				 */
				
				// Now we need to calculate which states would match this max price???
				for(State prevState : prevCellStates) {
					// do the calc here
					double priceTmp = prevMaxPrice + emissionProbabilitiesMatrix[GAP][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))]
							+ transitionProbabilitiesMatrix[prevState.getStateNum()][currState.getStateNum()];
					if( Math.abs(priceTmp - maxPrice) <= EPSILON ) {
						// One of the fitting states --> Pick it!
						prevMatchingState = prevState;
						break;
					}
				}
				break;
			case UP:
				prevCell = NWTable[i-1][j];
				prevCellStates = prevCell.getHighestScoreStates();
				prevMaxPrice = prevCell.getHighestPathScore();
				
				/*
				 * This is the map where we shall put the values into. What values you ask?
				 * Well we have to calculate the prices for 
				 */
				
				// Now we need to calculate which states would match this max price???
				for(State prevState : prevCellStates) {
					// do the calc here
					double priceTmp = prevMaxPrice + emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][GAP]
							+ transitionProbabilitiesMatrix[prevState.getStateNum()][currState.getStateNum()];
					if( Math.abs(priceTmp - maxPrice) <= EPSILON ) {
						// One of the fitting states --> Pick it!
						prevMatchingState = prevState;
						break;
					}
				}
				break;
			default:
				throw new IllegalArgumentException("Unallowed direction recorded!");
			}
			
			// Check if a prevMatchingState was found
			if(prevMatchingState == null) {
				throw new NullPointerException("No previous Cell State was found that would transit into this Cell State!");
			}
			
			// Now we have the current state, and the previous state. Therefore we can go backwards
			// Append the values
			String appendUpper = null;
			String appendLower = null;
			int newi = -1, newj = -1;
			switch(currState) {
			case MTCH:
				// Just fall down
			case MISS:
				appendUpper = String.valueOf(upperGaplessSeq.charAt(j));
				appendLower = String.valueOf(lowerGaplessSeq.charAt(i));
				
				newi = i - 1;
				newj = j - 1;
				
				break;
			case U:
			case X:
				appendUpper = "-";
				appendLower = String.valueOf(lowerGaplessSeq.charAt(i));
				
				newi = i - 1;
				newj = j;
				
				break;
			case L:
			case Y:
				appendUpper = String.valueOf(upperGaplessSeq.charAt(j));
				appendLower = "-";
				
				newi = i;
				newj = j - 1;
				
				break;
			case B:
			case E:
				// These should never happen???
				break;
			}
			
			StringBuilder upperSeq = sequencePair.get(PAIR_UPPER_SEQ_INDEX);
			StringBuilder lowerSeq = sequencePair.get(PAIR_LOWER_SEQ_INDEX);
			
			upperSeq.append(appendUpper);
			lowerSeq.append(appendLower);
			
			// Alright, break now, don't go further if the calculation is done
			return traceback(NWTable, newi, newj, upperGaplessSeq, lowerGaplessSeq, sequencePair, prevMatchingState);
		}
		
		return null;
	}
	
	/**
	 * Transforms the table's probability values into log values according to the given
	 * argument.
	 * 
	 * The base of the logarithm is set to e
	 * 
	 * @param mat The matrix over the elements of which will the logarithm be applied
	 */
	private static void logOddsMatrix(double[][] mat) {
		for(int i = 0; i < mat.length; i++) {
			for(int j = 0; j < mat[i].length; j++) {
				mat[i][j] = Math.log(mat[i][j]);
			}
		}
	}
	
	private static void izracunajVjerojatnostiTranzicija() {
		
		for(int i=0; i<TRANSITIONS_MATRIX_CONST; i++) {
			
			int ukupno=0;
			for(int j=0; j<TRANSITIONS_MATRIX_CONST; j++) {
				ukupno += transitionMatrix[i][j];
			}
			
			for(int j=0; j<TRANSITIONS_MATRIX_CONST; j++) {
				
				// Skip the row corresponding to the End state
				if(i!=1) transitionProbabilitiesMatrix[i][j] = (double)transitionMatrix[i][j] / ukupno;
			}
		}
	}



	private static void izracunajVjerojatnostiEmisija() {
		int ukupno = 0;
		
		//racunanje ukupne sume
		for(int i = 0; i < EMISSIONS_MATRIX_CONST - 1; i++) {
			for(int j = 0; j < EMISSIONS_MATRIX_CONST - 1; j++) {
				ukupno += emissionsMatrix[i][j];
			}
		}
		
		//racunanje vjerojatnosti za prva 4 reda i stupca (M)
		for(int i = 0; i < EMISSIONS_MATRIX_CONST - 1; i++) {
			for(int j = 0; j < EMISSIONS_MATRIX_CONST - 1; j++) {
				emissionProbabilitiesMatrix[i][j] = (double)emissionsMatrix[i][j] / ukupno;
			}
		}
		
		//racunanje vjerojatnosti za 5. stupac i redak
		int ukupnoRedakGap = 0;
		int ukupnoStupacGap = 0;
		
		// 5. stupac
		for(int i = 0; i < EMISSIONS_MATRIX_CONST - 1; i++) {
			ukupnoStupacGap+=emissionsMatrix[i][EMISSIONS_MATRIX_CONST-1]; // 5. stupac
			ukupnoRedakGap+=emissionsMatrix[EMISSIONS_MATRIX_CONST-1][i]; // 5. redak
		}
		
		for(int i = 0; i < EMISSIONS_MATRIX_CONST - 1; i++) {
			emissionProbabilitiesMatrix[i][EMISSIONS_MATRIX_CONST-1] = (double) emissionsMatrix[i][EMISSIONS_MATRIX_CONST-1] / ukupnoStupacGap;
			emissionProbabilitiesMatrix[EMISSIONS_MATRIX_CONST-1][i] = (double) emissionsMatrix[EMISSIONS_MATRIX_CONST-1][i] / ukupnoRedakGap;
		}
		
	}

	private static void ispisiMatricu(int[][] mat) {
		
		for(int i=0; i<mat.length; i++) {
			for(int j=0; j < mat[i].length; j++) {
				System.out.format("%10d ", mat[i][j]);
			}
			System.out.println();
		}
		System.out.println();
		
	}
	
	private static void ispisiMatricuVjerojatnosti(double[][] mat, String emisOrTrans) {
		String[]  identifiers;
		if(emisOrTrans.contentEquals("emis")) {
			identifiers = new String[] {"A", "C", "G", "T", "-"};
		} else if(emisOrTrans.contentEquals("trans")) {
			identifiers = new String[] {"Begin", "End", "Match", "MisMatch", "X(upper gap)", 
					"Y(lower gap)", "U(repeated upper gap)", "L(repeated lower gap)"};
		} else {
			throw new IllegalArgumentException("Invalid emisOrTrans argument.");
		}
		
		// Print horizontally
		System.out.format("%24s", "");
		for(int i = 0; i < identifiers.length; i++) {
		 System.out.format("%24s", identifiers[i]);	
		}
		System.out.println();		// Wanna add a separator between the beginning identifiers row and the rest of the table
		
		// Now also need to print vertically in each row
		for(int i=0; i<mat.length; i++) {
			// First goes the name
			System.out.format("%24s", identifiers[i]);
			
			for(int j=0; j<mat[i].length; j++) {
				System.out.format("%24.10f", mat[i][j]);
			}
			System.out.println();
		}
		System.out.println();
		
	}

	private static void napraviMatricuTranzicija(List<String> lines) {
		
		for(int i=0; i<lines.size()-1; i++) {
			for(int j=i+1; j<lines.size(); j++) {
				
				char[] redak1 = lines.get(i).toCharArray();
				char[] redak2 = lines.get(j).toCharArray();
				char prethodnoStanje='B';		// Begin state
				
				for(int k=0; k<redak1.length; k++) {
					char znak1=redak1[k];
					char znak2=redak2[k];
					
					switch(prethodnoStanje) {
					case 'B':
						if(znak1=='-' && znak2!='-') {
							transitionMatrix[0][4]++;
							prethodnoStanje='X';
						}
						else if(znak1!='-' && znak2=='-') {
							transitionMatrix[0][5]++;
							prethodnoStanje='Y';
						} else if(znak1!='-' && znak2!='-') {
							// Now check if they are equal
							if(znak1==znak2) {
								// Match
								transitionMatrix[0][2]++;
								prethodnoStanje='M';
							} else {
								// Mismatch
								transitionMatrix[0][3]++;
								prethodnoStanje='S';
							}
							
						}
						break;
					
					case 'M':
						if(znak1=='-' && znak2!='-') {
							transitionMatrix[2][4]++;
							prethodnoStanje='X';
						}
						else if(znak1!='-' && znak2=='-') {
							transitionMatrix[2][5]++;
							prethodnoStanje='Y';
						}
						else if(znak1!='-' && znak2!='-') {
							// Now check if they are equal
							if(znak1==znak2) {
								// Match
								transitionMatrix[2][2]++;
								prethodnoStanje='M';
							} else {
								// Mismatch
								transitionMatrix[2][3]++;
								prethodnoStanje='S';
							}
						}
						break;
						
					case 'S':				// Mismatch state
						if(znak1=='-' && znak2!='-') {
							transitionMatrix[3][4]++;
							prethodnoStanje='X';
						}
						else if(znak1!='-' && znak2=='-') {
							transitionMatrix[3][5]++;
							prethodnoStanje='Y';
						}
						else if(znak1!='-' && znak2!='-') {
							// Now check if they are equal
							if(znak1==znak2) {
								// Match
								transitionMatrix[3][2]++;
								prethodnoStanje='M';
							} else {
								// Mismatch
								transitionMatrix[3][3]++;
								prethodnoStanje='S';
							}
						}
						break;
						
					case 'X':
						if(znak1=='-' && znak2!='-') {
							transitionMatrix[4][6]++;
							prethodnoStanje='U';
						}
						else if(znak1!='-' && znak2=='-') {
							transitionMatrix[4][5]++;
							prethodnoStanje='Y';
						}
						else if(znak1!='-' && znak2!='-') {
							// Now check if they are equal
							if(znak1==znak2) {
								// Match
								transitionMatrix[4][2]++;
								prethodnoStanje='M';
							} else {
								// Mismatch
								transitionMatrix[4][3]++;
								prethodnoStanje='S';
							}
						}
						break;
						
					case 'Y':
						if(znak1=='-' && znak2!='-') {
							transitionMatrix[5][4]++;
							prethodnoStanje='X';
						}
						else if(znak1!='-' && znak2=='-') {
							transitionMatrix[5][7]++;
							prethodnoStanje='L';
						}
						else if(znak1!='-' && znak2!='-') {
							// Now check if they are equal
							if(znak1==znak2) {
								// Match
								transitionMatrix[5][2]++;
								prethodnoStanje='M';
							} else {
								// Mismatch
								transitionMatrix[5][3]++;
								prethodnoStanje='S';
							}
						}
						break;
					
					case 'U':
						if(znak1=='-' && znak2!='-') {
							transitionMatrix[6][6]++;
							prethodnoStanje='U';
						}
						else if(znak1!='-' && znak2=='-') {
							transitionMatrix[6][5]++;
							prethodnoStanje='Y';
						}
						else if(znak1!='-' && znak2!='-') {
							// Now check if they are equal
							if(znak1==znak2) {
								// Match
								transitionMatrix[6][2]++;
								prethodnoStanje='M';
							} else {
								// Mismatch
								transitionMatrix[6][3]++;
								prethodnoStanje='S';
							}
						}
						break;
					
					case 'L':
						if(znak1=='-' && znak2!='-') {
							transitionMatrix[7][4]++;
							prethodnoStanje='X';
						}
						else if(znak1!='-' && znak2=='-') {
							transitionMatrix[7][7]++;
							prethodnoStanje='L';
						}
						else if(znak1!='-' && znak2!='-') {
							// Now check if they are equal
							if(znak1==znak2) {
								// Match
								transitionMatrix[7][2]++;
								prethodnoStanje='M';
							} else {
								// Mismatch
								transitionMatrix[7][3]++;
								prethodnoStanje='S';
							}
						}
						break;
						
				}
					// Handling the End state
				     if(k==redak1.length-1) {
					      switch(prethodnoStanje) {
					      case 'B':
					    	  transitionMatrix[0][1]++; break;
					
					      case 'M':
					    	  transitionMatrix[2][1]++; break;
					
					      case 'S':
					    	  transitionMatrix[3][1]++; break;
					    	  
					      case 'X':
					    	  transitionMatrix[4][1]++; break;
						
					      case 'Y':
					    	  transitionMatrix[5][1]++; break;
					      
					      case 'U':
					    	  transitionMatrix[6][1]++; break;
					    	  
					      case 'L':
					    	  transitionMatrix[7][1]++; break;
					      }
				      }
			     }
		    }
		}
		
	}

	private static void napraviMatricuEmisija(List<String> lines) {
		
		for(int i=0; i<lines.size()-1; i++) {
			for(int j=i+1; j<lines.size(); j++) {
				
				char[] redak1 = lines.get(i).toCharArray();
				char[] redak2 = lines.get(j).toCharArray();
				
				for(int k=0; k<redak1.length; k++) {
					char znak1=redak1[k];
					char znak2=redak2[k];
					int prvi;
					int drugi;
					
					switch(znak1) {
					case 'A' :
						prvi=0; break;
					case 'C' :
						prvi=1; break;
					case 'G' :
						prvi=2; break;
					case 'T' :
						prvi=3; break;
					default :
						// The '-' case
						prvi=4;
					}
					
					
					switch(znak2) {
					case 'A' :
						drugi=0; break;
					case 'C' :
						drugi=1; break;
					case 'G' :
						drugi=2; break;
					case 'T' :
						drugi=3; break;
					default :
						// The '-' case
						drugi=4;
					}
					
					emissionsMatrix[prvi][drugi]++;
					emissionsMatrix[drugi][prvi]++;
						
				}
			}
		}
	}

	/**
	 * First reads the whole file into one massive String. This massive String is then accordingly edited into
	 * a list of Strings on which matrices are constructed
	 * 
	 * @param path The input file
	 */
	private static List<String> fileRazvrstajUPoljeStringova(Path path) {
		List<String> lines = new ArrayList<String>();
		StringBuilder sequence = new StringBuilder();
		boolean beginningSkip = true;
		try(Scanner sc = new Scanner(path)) {
			while(sc.hasNextLine()) {
				// Check if this line marks the beginning
				String line = sc.nextLine();
				if(line.startsWith(">Ref")) {
					/*
					 * End of the last sequence, and a beginning of the new one
					 * Save the last sequence into the list, and then clear the sequence
					 * StringBuilder, as well as increase the index.
					 * 
					 * Note: at index -1(the beginning) just skip the cycle
					 */
					if(!beginningSkip) {
						// If not beginning of file do the procedure described above
						lines.add(sequence.toString());
						sequence = new StringBuilder();
					} else {
						beginningSkip = false;
					}
					
					continue;
				}
				
				/*
				 * Else need to read this into the sequence StringBuilder -- which shall later on
				 * be added into the lines list of sequences
				 */
				sequence.append(line);
				
			}
			
			// Need to append the last sequence
			lines.add(sequence.toString());
	    }
	    catch(IOException e) {
	    	e.printStackTrace();
	    }
		
		return lines;
	}
	
}
