
public enum State {

	B(0, Direction.DIAGONAL_DOWN_LEFT),
	E(1, Direction.DIAGONAL_DOWN_RIGHT),
	MTCH(2, Direction.DIAGONAL_UP_LEFT),
	MISS(3, Direction.DIAGONAL_UP_LEFT),
	X(4, Direction.UP),
	Y(5, Direction.LEFT),
	U(6, Direction.UP),
	L(7, Direction.LEFT);
	
	private int stateNum;
	private Direction whereFrom;
	
	private State(int number, Direction whereFrom) {
		this.stateNum = number;
		this.whereFrom = whereFrom;
	}

	public int getStateNum() {
		return stateNum;
	}

	public Direction getWhereFrom() {
		return whereFrom;
	}
	
	public static State valueOf(Direction direction){
		for(State currState : State.values()) {
			if(direction.equals(currState.whereFrom)) {
				return currState;
			}
		}
		
		return null;
	}
	
	public static State valueOf(int number){
		for(State currState : State.values()) {
			if(number == currState.getStateNum()) {
				return currState;
			}
		}
		
		return null;
	}
}
