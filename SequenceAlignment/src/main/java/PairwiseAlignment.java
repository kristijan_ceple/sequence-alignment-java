import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Scanner;

public class PairwiseAlignment {

	public static int EMISSIONS_MATRIX_CONST = 5;		// A, T, G, C, and -
	public static int TRANSITIONS_MATRIX_CONST = 8; // B, E, MATCH. MISS, X, Y,
	// U (repeated upper gap) and L (repeated lower gap)

	public static String PATH_CONST = "HIV1_REF_2010_genome_DNA.fasta";
	//	public static String PATH_CONST = "C:/Users/Bruno/Desktop/hiv.txt";
	public static int GAP_POINTS = -1;
	public static int MATCH_POINTS = -2;

	//a je matrica emisija
	//b je matrica transformacija
	//c je matrica vjerojatnosti emisija
	//d je matrica vjerojatnosti tranzicija
	private static int emissionsMatrix[][] = new int[EMISSIONS_MATRIX_CONST][EMISSIONS_MATRIX_CONST];
	private static int transitionMatrix[][] = new int[TRANSITIONS_MATRIX_CONST][TRANSITIONS_MATRIX_CONST];
	private static double emissionProbabilitiesMatrix[][] = new double[EMISSIONS_MATRIX_CONST][EMISSIONS_MATRIX_CONST];
	private static double transitionProbabilitiesMatrix[][] = new double[TRANSITIONS_MATRIX_CONST][TRANSITIONS_MATRIX_CONST];

	private static List<String> lines = new ArrayList<String>();

	private static Map<String, Integer> transitionsMap = new HashMap<>();
	private static Map<String, Integer> emissionsMap = new HashMap<>();

	private static final int B = 0;
	private static final int E = 1;
	private static final int MTCH = 2;
	private static final int MISS= 3;
	private static final int X = 4;
	private static final int Y = 5;
	private static final int U = 6;
	private static final int L = 7;

	private static final int A = 0;
	private static final int C = 1;
	private static final int G = 2;
	private static final int T = 3;
	private static final int GAP = 4;

	private static final int PAIR_UPPER_SEQ_INDEX = 0;
	private static final int PAIR_LOWER_SEQ_INDEX = 1;

	public static void main(String[] args) {

		String file=PATH_CONST;
		Path path = Paths.get(file);

		fileRazvrstajUPoljeStringova(path);

//		System.out.println(lines.get(38));
//		System.out.println("Charlie");

		napraviMatricuEmisija();
		napraviMatricuTranzicija();

		System.out.println("Matrica emisija");
		ispisiMatricu(emissionsMatrix);

		System.out.println("Matrica tranzicija");
		ispisiMatricu(transitionMatrix);

		izracunajVjerojatnostiEmisija();
		System.out.println("Matrica vjerojatnosti emisija");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");

		izracunajVjerojatnostiTranzicija();
		System.out.println("Matrica vjerojatnosti tranzicija");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");

		Cell[][] NWTable = NeedlemanWunsch("CAAGAC", "GAAC");

		System.out.println("Jure");
	}

	@SuppressWarnings("preview")
	private static String gapOrChars(Direction toDecode) {
		return switch(toDecode) {
			case LEFT -> "Y";
			case DIAGONAL_UP_LEFT -> "CHARS";
			case UP -> "X";
			default -> throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
		};
	}

	@SuppressWarnings("preview")
	private static String decodeState(Direction toDecode, char upperChar, char lowerChar) {
		switch(toDecode) {
			case LEFT:
				return "Y";
			case DIAGONAL_UP_LEFT:
				return upperChar == lowerChar ? "MTCH" : "MISS";
			case UP:
				return "X";
			default:
				throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
		}
	}

	@SuppressWarnings("preview")
	private static Cell[][] NeedlemanWunsch(String upperSeq, String lowerSeq) {
		// Need to form a table first - find out its dimensions
		int ncol = upperSeq.length(), nrow = lowerSeq.length();
		Cell[][] NWTable = new Cell[nrow+1][ncol+1];		// This shall be used as the table

//		int matchPoints,
//			misMatchPoints,
//			gapXPoints,
//			gapYPoints;

		// First fill the transitionsMap with values
		transitionsMap.put("B", 0);
		transitionsMap.put("E", 1);
		transitionsMap.put("MTCH", 2);
		transitionsMap.put("MISS", 3);
		transitionsMap.put("X", 4);
		transitionsMap.put("Y", 5);
		transitionsMap.put("U", 6);
		transitionsMap.put("L", 7);


		// Now fill the emissionsMap with values
		emissionsMap.put("A", 0);
		emissionsMap.put("C", 1);
		emissionsMap.put("G", 2);
		emissionsMap.put("T", 3);
		emissionsMap.put("-", 4);

		// Need to make log tables now
		logOddsMatrix(emissionProbabilitiesMatrix);
		logOddsMatrix(transitionProbabilitiesMatrix);

		System.out.println("Matrica vjerojatnosti emisija");
		ispisiMatricuVjerojatnosti(emissionProbabilitiesMatrix, "emis");

		System.out.println("Matrica vjerojatnosti tranzicija");
		ispisiMatricuVjerojatnosti(transitionProbabilitiesMatrix, "trans");

		// Let's first initialise the Cells
		for(int i = 0; i <= nrow; i++) {
			for(int j = 0; j <= ncol; j++) {
				NWTable[i][j] = new Cell();
			}
		}

		// The algorithm calculation part
		/*
		 * We start in the Begin state, and are located in the upper leftmost cell. From there, we calculate
		 * first the 0th row and the 0th column, and then move on to the others
		 */
		String currentState = "B";
		String nextState;

		String upperGaplessSeq = upperSeq.replaceAll("-", "");
		String lowerGaplessSeq = lowerSeq.replaceAll("-", "");

		NWTable[0][0].setHighestPathScore(0);

		// ��������������������� Let's fill the first row	����������������������//
		nextState = "Y";
		NWTable[0][1].setHighestPathScore( 				// B -> Y
				transitionProbabilitiesMatrix[transitionsMap.get(currentState)][transitionsMap.get(nextState)]
						+ emissionProbabilitiesMatrix[emissionsMap.get("-")][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(0)))]
		);
		currentState = "Y";		// Consecutive gaps!
		nextState = "L";
		NWTable[0][2].setHighestPathScore(
				transitionProbabilitiesMatrix[transitionsMap.get(currentState)][transitionsMap.get(nextState)]
						+ emissionProbabilitiesMatrix[emissionsMap.get("-")][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(1)))]
						+ NWTable[0][1].getHighestPathScore()
		);

		// Now we're in state L, and the next state is L as well --> CONSECUTIVE GAPS
		currentState = nextState;
		for( int j = 2; j < ncol; j++) {
			// The gaps in the lower sequence(transitions L -> L)
			NWTable[0][j+1].setHighestPathScore(
					transitionProbabilitiesMatrix[transitionsMap.get(currentState)][transitionsMap.get(nextState)]
							+ emissionProbabilitiesMatrix[emissionsMap.get("-")][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))]
							+ NWTable[0][j].getHighestPathScore()
			);
		}

		// ��������������������� Let's fill the first column	����������������������//
		currentState = "B";
		nextState = "X";
		NWTable[1][0].setHighestPathScore( 				// B -> X
				transitionProbabilitiesMatrix[transitionsMap.get(currentState)][transitionsMap.get(nextState)]
						+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(0)))][emissionsMap.get("-")]
		);

		currentState = "X";
		nextState = "U";	// Next state shall be consecutive gaps
		NWTable[2][0].setHighestPathScore(
				transitionProbabilitiesMatrix[transitionsMap.get(currentState)][transitionsMap.get(nextState)]
						+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(1)))][emissionsMap.get("-")]
						+ NWTable[1][0].getHighestPathScore()
		);

		// Now we're in state U, and the next state is U as well --> CONSECUTIVE GAPS
		currentState = nextState;
		for( int i = 2; i < nrow; i++ ) {
			NWTable[i+1][0].setHighestPathScore(
					transitionProbabilitiesMatrix[transitionsMap.get(currentState)][transitionsMap.get(nextState)]
							+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][emissionsMap.get("-")]
							+ NWTable[i][0].getHighestPathScore()
			);
		}

		currentState = "B";		// Set the currentState to the beginning state
		// Time to go row by row and parse each cell - gather 3 values, and select the maximum of them
		for(int i = 1; i <= nrow; i++) {
			for(int j = 1; j <= ncol; j++) {
				// Do the algo here
				/*
				 * This is the algorithm:
				 * 1. Depending whether it's about fromLeft, fromTop or fromDiag - get that cell's cameFrom map.
				 * Do this for all the 3 neigbouring Cells.
				 * 2. Need to iterate over this map:
				 * 	a) Discover that Cell's State using the Direction key of the map
				 * 	b) Calculate the total corresponding price for the current Cell
				 * 	c) Note the price and the path down into current Cell's cameFromWithPrices map
				 *
				 * 3. After this has been done for all 3 neighbouring Cells, it is time to find the maximum price.
				 * That price needs to be noted down in this cell's cameFrom map.
				 *
				 * Note the difference between the cameFrom and cameFromWithPrices maps. The cameFrom map features only the paths
				 * with the highest(and in this case, equal) price, while the cameFromWithPrices map features all the paths ever tested and calculated.
				 */


// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Let's do the LEFT case		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
				/*
				 * If we moved from the left, that means there was a gap in the lower sequence
				 * Transition from prevCellStates -> Y.
				 */
				// 1. fromLeft --> get the cell and its bestPathsWithouPrices map
				Map<Direction, List<Direction>> prevCellPaths = NWTable[i][j-1].getBestPathsWithoutPrices();
				Map<Direction, Map<Direction, Double>> allPathsWithPrices = NWTable[i][j].getAllPathsWithPrices();
				allPathsWithPrices.put(Direction.LEFT, new HashMap<>());		// Initialising the map

				// 2. Iterate over that map's entries
				for(Entry<Direction, List<Direction>> prevCellCurrEntry : prevCellPaths.entrySet()) {
					// 2.a) Get the State from the Direction key
					// Remember, GAP IN THE LOWER SEQUENCE!!!
					String prevCellState = decodeState(prevCellCurrEntry.getKey(), upperGaplessSeq.charAt(j), '-');
					String currCellState = "Y";

					// 2.b) Calculate the price
					Double fromLeft = NWTable[i][j-1].getHighestPathScore();
					double price = fromLeft
							+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
							+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))][GAP];

					// End state handling
					if(i == nrow && j == ncol) {
						price += transitionProbabilitiesMatrix[transitionsMap.get(currCellState)][E];
					}

					// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
					allPathsWithPrices.get(Direction.LEFT).put(prevCellCurrEntry.getKey(), price);
				}


// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Let's do the TOP case		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
				/*
				 * If we moved from the top, that means there was a gap in the upper sequence
				 * Transition from prevCellStates -> X.
				 */

				// 1. fromTop --> get the cell and its bestPathsWithouPrices map
				prevCellPaths = NWTable[i-1][j].getBestPathsWithoutPrices();
				allPathsWithPrices.put(Direction.UP, new HashMap<>());		// Initialising the map

				// 2. Iterate over that map's entries
				for(Entry<Direction, List<Direction>> prevCellCurrEntry : prevCellPaths.entrySet()) {
					// 2.a) Get the State from the Direction key
					String prevCellState = decodeState(prevCellCurrEntry.getKey(), upperGaplessSeq.charAt(j), lowerGaplessSeq.charAt(i));
					String currCellState = "X";

					// 2.b) Calculate the price
					Double fromTop = NWTable[i-1][j].getHighestPathScore();
					double price = fromTop
							+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
							+ emissionProbabilitiesMatrix[GAP][emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))];

					// End state handling
					if(i == nrow && j == ncol) {
						price += transitionProbabilitiesMatrix[transitionsMap.get(currCellState)][E];
					}

					// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
					allPathsWithPrices.get(Direction.UP).put(prevCellCurrEntry.getKey(), price);
				}


// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Let's do the DIAGONAL case		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
				// Check if the NWTable[1][1] - then we have to handle this a lil' bit differently!

				prevCellPaths = NWTable[i-1][j-1].getBestPathsWithoutPrices();
				NWTable[i][j].getAllPathsWithPrices().put(Direction.DIAGONAL_UP_LEFT, new HashMap<>());		// Initialising the map

				// Need to see whether it's a match, or a mismatch! --> for the next State
				// 1. fromDiag --> get the cell and its bestPathsWithouPrices map
				if(i == 1 && j == 1) {
					String prevCellState = "B";
					boolean condition = upperGaplessSeq.charAt(j) == lowerGaplessSeq.charAt(i);
					String currCellState = condition ? "MTCH" : "MISS";

					// 2.b) Calculate the price
					Double fromDiag = NWTable[i-1][j-1].getHighestPathScore();
					double price = fromDiag
							+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
							+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))];

					// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
					allPathsWithPrices.get(Direction.DIAGONAL_UP_LEFT).put(Direction.DIAGONAL_DOWN_RIGHT, price);		// DIAGONAL_DOWN_RIGHT is being used as to make this Begin Cell unique
				} else {
					// 2. Iterate over that map's entries
					for(Entry<Direction, List<Direction>> prevCellCurrEntry : prevCellPaths.entrySet()) {
						// 2.a) Get the State from the Direction key
						String prevCellState = decodeState(prevCellCurrEntry.getKey(), upperGaplessSeq.charAt(j), lowerGaplessSeq.charAt(i));
						// Match or mismatch?
						boolean condition = upperGaplessSeq.charAt(j) == lowerGaplessSeq.charAt(i);
						String currCellState = condition ? "MTCH" : "MISS";

						// 2.b) Calculate the price
						Double fromDiag = NWTable[i-1][j-1].getHighestPathScore();
						double price = fromDiag
								+ transitionProbabilitiesMatrix[transitionsMap.get(prevCellState)][transitionsMap.get(currCellState)]
								+ emissionProbabilitiesMatrix[emissionsMap.get(String.valueOf(lowerGaplessSeq.charAt(i)))][emissionsMap.get(String.valueOf(upperGaplessSeq.charAt(j)))];

						// End state handling
						if(i == nrow && j == ncol) {
							price += transitionProbabilitiesMatrix[transitionsMap.get(currCellState)][E];
						}

						// 2.c) Excellent, now we have the price. Time to form this new entry and add it down into the all paths with prices map
						allPathsWithPrices.get(Direction.DIAGONAL_UP_LEFT).put(prevCellCurrEntry.getKey(), price);
					}
				}

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		ON TO STEP 3		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
				// 3. Need to go through all the Paths now. Find the ones with the biggest price, and put them into the specialised
				// map which contains only the max-price Paths

				// First pass to discover the max Price
				Optional<Double> maxPriceOptional = allPathsWithPrices.values().parallelStream()
						.map(Map::values)
						.flatMap(Collection::parallelStream)
						.max(Double::compare);

				if(maxPriceOptional.isEmpty()) {
					// maxPrice not present -- what? This is obviously some weird edge case
					// that should be further inspected
					throw new RuntimeException("Empty/Not present maxPrice?");
				}
				double maxPrice = maxPriceOptional.get();

				// Second pass to add the best Paths
				Map<Direction, List<Direction>> bestPathsWithoutPrices = NWTable[i][j].getBestPathsWithoutPrices();
				// Let's now find the Paths that correspond to this newfound max price
				// Have to check each miniMap
				for(Entry<Direction, Map<Direction, Double>> entry : allPathsWithPrices.entrySet()) {
					// Go through each minimap
					for(Entry<Direction, Double> subEntry : entry.getValue().entrySet()) {
						if(subEntry.getValue().equals(maxPrice)) {
							// Add it to the bestPaths!
							bestPathsWithoutPrices.compute(entry.getKey(), (k, v) -> {
								Direction subDirection = subEntry.getKey();

								if(v == null) {
									List<Direction> toPutMap = new ArrayList<>();
									toPutMap.add(subDirection);
									return toPutMap;
								} else {
									v.add(subDirection);
									return v;
								}
							});
						}
					}
				}

				NWTable[i][j].setHighestPathScore(maxPrice);
			}
		}


// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		Traceback		$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

		// Once that is done we will have the table filled. Now need to follow those directions back
		List<List<String>> stringPairs = traceback(NWTable, nrow, ncol, upperGaplessSeq, lowerGaplessSeq,
				new ArrayList<>(),
				new ArrayList<>());

		// Reversing the traceback
		for(List<String> pair : stringPairs) {
			// One possible alignment. Reverse the sequences!
			String upperSeqTmp = pair.get(PAIR_UPPER_SEQ_INDEX);
			String lowerSeqTmp = pair.get(PAIR_LOWER_SEQ_INDEX);

			String upperSeqTmpReversed = "";
			for(int i = upperSeqTmp.length() - 1; i > 0; i--) {
				upperSeqTmpReversed.concat(String.valueOf(upperSeqTmp.charAt(i)));
			}

			String lowerSeqTmpReversed = "";
			for(int i = upperSeqTmp.length() - 1; i > 0; i--) {
				lowerSeqTmpReversed.concat(String.valueOf(lowerSeqTmp.charAt(i)));
			}

			// Now we got 'em reversed. Time to put them in their place. Literally lol
			pair.set(PAIR_UPPER_SEQ_INDEX, upperSeqTmpReversed);
			pair.set(PAIR_LOWER_SEQ_INDEX, lowerSeqTmpReversed);
		}

		// Just print them out now
		System.out.println("Possible paths:");
		for(List<String> pair : stringPairs) {
			System.out.println(pair.get(PAIR_UPPER_SEQ_INDEX));
			System.out.println(pair.get(PAIR_LOWER_SEQ_INDEX));

			// Let's print the delimiter
			System.out.println("\n");
		}

		return NWTable;
	}

	/**
	 * Goes back. Generates a list of String pairs. In SequencePair, on index 0 lies the upper sequence,
	 * and on index 1 lies the lower sequence.
	 * @param nWTable
	 */
	@SuppressWarnings("preview")
	private static List<List<String>> traceback(Cell[][] NWTable, int i, int j, String upperGaplessSeq, String lowerGaplessSeq, List<List<String>> sequencePairs, List<String> sequencePair) {
		// The breaking statement - check if we've arrived to the beginning
		if(i == 0 && j == 0) {
			// Now add the String pair to the list of string pairs, and then return it
			sequencePairs.add(sequencePair);
			return sequencePairs;
		}

		Cell currentCell = NWTable[i][j];
		Map<Direction, List<Direction>> bestPaths = currentCell.getBestPathsWithoutPrices();
		// Let's follow now ehh

		// First check whether there's some branching happening
		boolean branch = false;
		if(bestPaths.size() > 1) {
			// Oh go there's a branching happening
			/*
			 * What's the point of branching you might ask?
			 * That much is clear --> in order to form different Paths. The better question is: how to achieve that? What must be done? Well we have to initialise
			 * two new Strings, and copy into them the content of the String that the traceback has received. This is because 1 Path is actually branching into 2
			 * new Paths, and therefore we need to preserve the pre-branch part, while still allowing the Paths to be 2/3 new Strings to which it is possible to
			 * concatenate onto. At the end of the traceback the recursion shall add all these paths to the List.
			 */
			branch = true;
		}

		for(Entry<Direction, List<Direction>> entry : bestPaths.entrySet()) {
			String state = gapOrChars(entry.getKey());

			// Will have to handle branching here. If there's branching happening we will need 2/3 new Pairs.
			List<String> pairSeqRef = sequencePair;

			if(branch) {
				// Let's make a new Pair for each of the entries
				List<String> newPair = new ArrayList<>();
				String newUpper = "", newLower = "";

				newUpper.concat(sequencePair.get(PAIR_UPPER_SEQ_INDEX));
				newLower.concat(sequencePair.get(PAIR_LOWER_SEQ_INDEX));

				// Okay, got em now. Update the reference to the new Pair
				pairSeqRef = newPair;
			}

			switch(state) {
				case "Y":
					pairSeqRef.get(PAIR_UPPER_SEQ_INDEX).concat(String.valueOf(upperGaplessSeq.charAt(j-1)));
					pairSeqRef.get(PAIR_LOWER_SEQ_INDEX).concat("-");	// gap lower
					break;
				case "X":
					pairSeqRef.get(PAIR_UPPER_SEQ_INDEX).concat("-");
					pairSeqRef.get(PAIR_LOWER_SEQ_INDEX).concat(String.valueOf(lowerGaplessSeq.charAt(i-1)));
					break;	// gap upper
				case "CHARS":
					pairSeqRef.get(PAIR_UPPER_SEQ_INDEX).concat(String.valueOf(upperGaplessSeq.charAt(j-1)));
					pairSeqRef.get(PAIR_LOWER_SEQ_INDEX).concat(String.valueOf(lowerGaplessSeq.charAt(i-1)));
					break;
				default:
					throw new RuntimeException("gapOrChars weird return - should not ever happen");
			}

			// We've concatenated the Paths right now. Continue the recursion now
			// Depending on the Direction, need to calculate the new i and j values
			Direction toCalculate = entry.getKey();
			int newi = i, newj = j;
			switch(toCalculate) {
				case LEFT:
					newj = j - 1;
					break;
				case DIAGONAL_UP_LEFT:
					newi = i -1; newj = j - 1;
					break;
				case UP:
					newi = i - 1;
					break;
				default:
					throw new IllegalArgumentException("Unallowed direction. Only south-wards-east-wards movement is allowed.");
			}

			return traceback(NWTable, newi, newj, upperGaplessSeq, lowerGaplessSeq, sequencePairs, pairSeqRef);
		}

		return null;
	}

	/**
	 * Transforms the table's probability values into log values according to the given
	 * argument.
	 *
	 * The base of the logarithm is set to e
	 *
	 * @param mat The matrix over the elements of which will the logarithm be applied
	 */
	private static void logOddsMatrix(double[][] mat) {
		for(int i = 0; i < mat.length; i++) {
			for(int j = 0; j < mat[i].length; j++) {
				mat[i][j] = Math.log(mat[i][j]);
			}
		}
	}

	private static void izracunajVjerojatnostiTranzicija() {

		for(int i=0; i<TRANSITIONS_MATRIX_CONST; i++) {

			int ukupno=0;
			for(int j=0; j<TRANSITIONS_MATRIX_CONST; j++) {
				ukupno += transitionMatrix[i][j];
			}

			for(int j=0; j<TRANSITIONS_MATRIX_CONST; j++) {

				// Skip the row corresponding to the End state
				if(i!=1) transitionProbabilitiesMatrix[i][j] = (double)transitionMatrix[i][j] / ukupno;
			}
		}
	}



	private static void izracunajVjerojatnostiEmisija() {
		int ukupno=0;

		//racunanje ukupne sume
		for(int i=0; i<EMISSIONS_MATRIX_CONST-1; i++) {
			for(int j=0; j<EMISSIONS_MATRIX_CONST-1; j++) {
				ukupno += emissionsMatrix[i][j];
			}
		}

		//racunanje vjerojatnosti za prva 4 reda i stupca (M)
		for(int i=0; i<EMISSIONS_MATRIX_CONST-1; i++) {
			for(int j=0; j<EMISSIONS_MATRIX_CONST-1; j++) {
				emissionProbabilitiesMatrix[i][j] = (double)emissionsMatrix[i][j] / ukupno;
			}
		}

		//racunanje vjerojatnosti za 5. stupac i redak
		int ukupnoRedakGap = 0;
		int ukupnoStupacGap = 0;

		// 5. stupac
		for(int i=0; i<EMISSIONS_MATRIX_CONST-1; i++) {
			ukupnoStupacGap+=emissionsMatrix[i][EMISSIONS_MATRIX_CONST-1]; // 5. stupac
			ukupnoRedakGap+=emissionsMatrix[EMISSIONS_MATRIX_CONST-1][i]; // 5. redak
		}

		for(int i=0; i<EMISSIONS_MATRIX_CONST-1; i++) {
			emissionProbabilitiesMatrix[i][EMISSIONS_MATRIX_CONST-1] = (double) emissionsMatrix[i][EMISSIONS_MATRIX_CONST-1] / ukupnoStupacGap;
			emissionProbabilitiesMatrix[EMISSIONS_MATRIX_CONST-1][i] = (double) emissionsMatrix[EMISSIONS_MATRIX_CONST-1][i] / ukupnoRedakGap;
		}

	}

	private static void ispisiMatricu(int[][] mat) {

		for(int i=0; i<mat.length; i++) {
			for(int j=0; j < mat[i].length; j++) {
				System.out.format("%3d ", mat[i][j]);
			}
			System.out.println();
		}
		System.out.println();

	}

	private static void ispisiMatricuVjerojatnosti(double[][] mat, String emisOrTrans) {
		String[]  identifiers;
		if(emisOrTrans.contentEquals("emis")) {
			identifiers = new String[] {"A", "C", "G", "T", "-"};
		} else if(emisOrTrans.contentEquals("trans")) {
			identifiers = new String[] {"Begin", "End", "Match", "MisMatch", "X(upper gap)",
					"Y(lower gap)", "U(repeated upper gap)", "L(repeated lower gap)"};
		} else {
			throw new IllegalArgumentException("Invalid emisOrTrans argument.");
		}

		// Print horizontally
		System.out.format("%24s", "");
		for(int i = 0; i < identifiers.length; i++) {
			System.out.format("%24s", identifiers[i]);
		}
		System.out.println();		// Wanna add a separator between the beginning identifiers row and the rest of the table

		// Now also need to print vertically in each row
		for(int i=0; i<mat.length; i++) {
			// First goes the name
			System.out.format("%24s", identifiers[i]);

			for(int j=0; j<mat[i].length; j++) {
				System.out.format("%24.10f", mat[i][j]);
			}
			System.out.println();
		}
		System.out.println();

	}

	private static void napraviMatricuTranzicija() {

		for(int i=0; i<lines.size()-1; i++) {
			for(int j=i+1; j<lines.size(); j++) {

				char[] redak1 = lines.get(i).toCharArray();
				char[] redak2 = lines.get(j).toCharArray();
				char prethodnoStanje='B';		// Begin state

				for(int k=0; k<redak1.length; k++) {
					char znak1=redak1[k];
					char znak2=redak2[k];

					switch(prethodnoStanje) {
						case 'B':
							if(znak1=='-' && znak2!='-') {
								transitionMatrix[0][4]++;
								prethodnoStanje='X';
							}
							else if(znak1!='-' && znak2=='-') {
								transitionMatrix[0][5]++;
								prethodnoStanje='Y';
							} else if(znak1!='-' && znak2!='-') {
								// Now check if they are equal
								if(znak1==znak2) {
									// Match
									transitionMatrix[0][2]++;
									prethodnoStanje='M';
								} else {
									// Mismatch
									transitionMatrix[0][3]++;
									prethodnoStanje='S';
								}

							}
							break;

						case 'M':
							if(znak1=='-' && znak2!='-') {
								transitionMatrix[2][4]++;
								prethodnoStanje='X';
							}
							else if(znak1!='-' && znak2=='-') {
								transitionMatrix[2][5]++;
								prethodnoStanje='Y';
							}
							else if(znak1!='-' && znak2!='-') {
								// Now check if they are equal
								if(znak1==znak2) {
									// Match
									transitionMatrix[2][2]++;
									prethodnoStanje='M';
								} else {
									// Mismatch
									transitionMatrix[2][3]++;
									prethodnoStanje='S';
								}
							}
							break;

						case 'S':				// Mismatch state
							if(znak1=='-' && znak2!='-') {
								transitionMatrix[3][4]++;
								prethodnoStanje='X';
							}
							else if(znak1!='-' && znak2=='-') {
								transitionMatrix[3][5]++;
								prethodnoStanje='Y';
							}
							else if(znak1!='-' && znak2!='-') {
								// Now check if they are equal
								if(znak1==znak2) {
									// Match
									transitionMatrix[3][2]++;
									prethodnoStanje='M';
								} else {
									// Mismatch
									transitionMatrix[3][3]++;
									prethodnoStanje='S';
								}
							}
							break;

						case 'X':
							if(znak1=='-' && znak2!='-') {
								transitionMatrix[4][6]++;
								prethodnoStanje='U';
							}
							else if(znak1!='-' && znak2=='-') {
								transitionMatrix[4][5]++;
								prethodnoStanje='Y';
							}
							else if(znak1!='-' && znak2!='-') {
								// Now check if they are equal
								if(znak1==znak2) {
									// Match
									transitionMatrix[4][2]++;
									prethodnoStanje='M';
								} else {
									// Mismatch
									transitionMatrix[4][3]++;
									prethodnoStanje='S';
								}
							}
							break;

						case 'Y':
							if(znak1=='-' && znak2!='-') {
								transitionMatrix[5][4]++;
								prethodnoStanje='X';
							}
							else if(znak1!='-' && znak2=='-') {
								transitionMatrix[5][7]++;
								prethodnoStanje='L';
							}
							else if(znak1!='-' && znak2!='-') {
								// Now check if they are equal
								if(znak1==znak2) {
									// Match
									transitionMatrix[5][2]++;
									prethodnoStanje='M';
								} else {
									// Mismatch
									transitionMatrix[5][3]++;
									prethodnoStanje='S';
								}
							}
							break;

						case 'U':
							if(znak1=='-' && znak2!='-') {
								transitionMatrix[6][6]++;
								prethodnoStanje='U';
							}
							else if(znak1!='-' && znak2=='-') {
								transitionMatrix[6][5]++;
								prethodnoStanje='Y';
							}
							else if(znak1!='-' && znak2!='-') {
								// Now check if they are equal
								if(znak1==znak2) {
									// Match
									transitionMatrix[6][2]++;
									prethodnoStanje='M';
								} else {
									// Mismatch
									transitionMatrix[6][3]++;
									prethodnoStanje='S';
								}
							}
							break;

						case 'L':
							if(znak1=='-' && znak2!='-') {
								transitionMatrix[7][4]++;
								prethodnoStanje='X';
							}
							else if(znak1!='-' && znak2=='-') {
								transitionMatrix[7][7]++;
								prethodnoStanje='L';
							}
							else if(znak1!='-' && znak2!='-') {
								// Now check if they are equal
								if(znak1==znak2) {
									// Match
									transitionMatrix[7][2]++;
									prethodnoStanje='M';
								} else {
									// Mismatch
									transitionMatrix[7][3]++;
									prethodnoStanje='S';
								}
							}
							break;

					}
					// Handling the End state
					if(k==redak1.length-1) {
						switch(prethodnoStanje) {
							case 'B':
								transitionMatrix[0][1]++; break;

							case 'M':
								transitionMatrix[2][1]++; break;

							case 'S':
								transitionMatrix[3][1]++; break;

							case 'X':
								transitionMatrix[4][1]++; break;

							case 'Y':
								transitionMatrix[5][1]++; break;

							case 'U':
								transitionMatrix[6][1]++; break;

							case 'L':
								transitionMatrix[7][1]++; break;
						}
					}
				}
			}
		}

	}

	private static void napraviMatricuEmisija() {

		for(int i=0; i<lines.size()-1; i++) {
			for(int j=i+1; j<lines.size(); j++) {

				char[] redak1 = lines.get(i).toCharArray();
				char[] redak2 = lines.get(j).toCharArray();

				for(int k=0; k<redak1.length; k++) {
					char znak1=redak1[k];
					char znak2=redak2[k];
					int prvi;
					int drugi;

					switch(znak1) {
						case 'A' :
							prvi=0; break;
						case 'C' :
							prvi=1; break;
						case 'G' :
							prvi=2; break;
						case 'T' :
							prvi=3; break;
						default :
							// The '-' case
							prvi=4;
					}


					switch(znak2) {
						case 'A' :
							drugi=0; break;
						case 'C' :
							drugi=1; break;
						case 'G' :
							drugi=2; break;
						case 'T' :
							drugi=3; break;
						default :
							// The '-' case
							drugi=4;
					}


					emissionsMatrix[prvi][drugi]++;
					emissionsMatrix[drugi][prvi]++;

				}
			}
		}
	}

	/**
	 * First reads the whole file into one massive String. This massive String is then accordingly edited into
	 * a list of Strings on which matrices are constructed
	 *
	 * @param path The input file
	 */
	private static void fileRazvrstajUPoljeStringova(Path path) {

		StringBuilder sequence = new StringBuilder();
		boolean beginningSkip = true;
		try(Scanner sc = new Scanner(path)) {
			while(sc.hasNextLine()) {
				// Check if this line marks the beginning
				String line = sc.nextLine();
				if(line.startsWith(">Ref")) {
					/*
					 * End of the last sequence, and a beginning of the new one
					 * Save the last sequence into the list, and then clear the sequence
					 * StringBuilder, as well as increase the index.
					 *
					 * Note: at index -1(the beginning) just skip the cycle
					 */
					if(!beginningSkip) {
						// If not beginning of file do the procedure described above
						lines.add(sequence.toString());
						sequence = new StringBuilder();
					} else {
						beginningSkip = false;
					}

					continue;
				}

				/*
				 * Else need to read this into the sequence StringBuilder -- which shall later on
				 * be added into the lines list of sequences
				 */
				sequence.append(line);

			}

			// Need to append the last sequence
			lines.add(sequence.toString());
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

}
