import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Used for the NW table. Stores data in a Map.
 * @author kikyy99
 *
 */
public class Cell {
	
	private double highestPathScore;
	
	/**
	 * Used instead of the backwards pointer -- helps when tracing back - Memoisation. Features only the directions
	 * with the highest price.
	 * 
	 * The first Direction indicates from where did we arrive to the current Cell.
	 * The second Direction indicates from where did we arrive into the previous cells - the ones mentioned above.
	 * This allows us to trace back whole Paths. It is important that we do this in order to keep the State transitions
	 * in order. -- This explanation is incomplete and doesn't probably help too much. Unfortunately I can't find
	 * a way to better explain it in such a short comment. :/ :(
	 */
	Map<Direction, List<Direction>> bestPathsWithoutPrices = new HashMap<>();

	/**
	 * Features ALL the directions, along with their prices.
	 */
	Map<Direction, Map<Direction, Double>> allPathsWithPrices = new HashMap<>();
	
	/**
	 * Empty constructor
	 */
	public Cell() {
		super();
	}

	/**
	 * Constructor with a single value that will be bound to this cell(reference saving)
	 * 
	 * @param val The value whose reference will be saved
	 */
	public Cell(double highestPathScore) {
		this.highestPathScore = highestPathScore;
	}

	public double getHighestPathScore() {
		return highestPathScore;
	}

	public Map<Direction, List<Direction>> getBestPathsWithoutPrices() {
		return bestPathsWithoutPrices;
	}

	public void setBestPathsWithoutPrices(Map<Direction, List<Direction>> bestPathsWithoutPrices) {
		this.bestPathsWithoutPrices.putAll(bestPathsWithoutPrices);
	}

	public Map<Direction, Map<Direction, Double>> getAllPathsWithPrices() {
		return allPathsWithPrices;
	}

	public void setAllPathsWithPrices(Map<Direction, Map<Direction, Double>> allPathsWithPrices) {
		this.allPathsWithPrices.putAll(allPathsWithPrices);
	}

	public void setHighestPathScore(double highestPathScore) {
		this.highestPathScore = highestPathScore;
	}

	/**
	 * Delegating method for cameFrom map handling.
	 * 
	 * @param firstLayerDirections The direction from where we came into the current cell. The cell can contain multiple firstLayerDirections
	 * should the prices be the same. That's why you can put multiple entries into the map
	 * 
	 * @param secondLayerDirections The previous cell's(from where we came into this cell using the firstLayerDirection)
	 * corresponding Paths - where did we come from into those previous cells? That's what the secondLayerDirections is used for.
	 * It's an array because, if the price is the same, multiple Paths are possible.
	 * 
	 */
	public void putDirections(Direction firstLayerDirection, Direction...secondLayerDirections) {
		if(secondLayerDirections == null) {
			throw new IllegalArgumentException("Second layer directions missing!");
		}
		
		this.bestPathsWithoutPrices.put(firstLayerDirection, Arrays.asList(secondLayerDirections));
	}

	@Override
	public String toString() {
		return "Cell [highestPathScore=" + highestPathScore + ", bestPathsWithoutPrices=" + bestPathsWithoutPrices
				+ ", allPathsWithPrices=" + allPathsWithPrices + "]";
	}
}
