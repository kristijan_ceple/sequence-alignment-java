
/**
 * Used to describe directions.
 * @author kikyy99
 */
public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	DIAGONAL_UP_LEFT,
	DIAGONAL_UP_RIGHT,
	DIAGONAL_DOWN_LEFT,
	DIAGONAL_DOWN_RIGHT;
}
